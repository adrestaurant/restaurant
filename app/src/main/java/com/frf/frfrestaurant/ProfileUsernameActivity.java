package com.frf.frfrestaurant;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class ProfileUsernameActivity extends AppCompatActivity {

    private EditText username;
    private Button updateButton;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    private JSONArray uname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_username);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        username = findViewById(R.id.username);

        updateButton = findViewById(R.id.updateButton);


        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    update();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        new FetchUsernameAsyncTask().execute();

    }


    private class FetchUsernameAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ProfileUsernameActivity.this)) {
                Toast.makeText(ProfileUsernameActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ProfileUsernameActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEUSERNAMEINDEX);

                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();

            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("usernamejson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            if (parseContent.isSuccess(result)) {
                uname = parseContent.getData((result));
                JSONObject name = null;
                try {
                    name = uname.getJSONObject(0);
                    username.setText( Html.fromHtml(name.getString(AndyConstants.Params.USERNAME)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {//if something goes wrong with the retrieval, put them back at the login screen with an error toast
                Toast.makeText(ProfileUsernameActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileUsernameActivity.this, ProfileHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                ProfileUsernameActivity.this.finish();
            }
            Log.d("username", String.valueOf(username));
        }
    }

    public void update()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(ProfileUsernameActivity.this)) {
            Toast.makeText(ProfileUsernameActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileUsernameActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.USERNAME, username.getText().toString());


        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEUSERNAMEUPDATE);

                    map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result);
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {
            Toast.makeText(ProfileUsernameActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();//not an error message, it tells them it was successful and they should login again

            preferenceHelper.clearPrefs();//clear all preferences (log them out)

            Intent intent = new Intent(ProfileUsernameActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }else {
            Toast.makeText(ProfileUsernameActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }

}
