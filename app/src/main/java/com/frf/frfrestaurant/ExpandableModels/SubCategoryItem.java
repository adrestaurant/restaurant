package com.frf.frfrestaurant.ExpandableModels;

import android.text.Html;

public class SubCategoryItem {

    private String categoryId;
    private String subId;
    private String subCategoryName;
    private String isChecked;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId =  String.valueOf(Html.fromHtml(categoryId));
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = String.valueOf(Html.fromHtml(subCategoryName));
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }


}
