package com.frf.frfrestaurant;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.ExpandableAdapters.ParishExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableAdapters.VillageExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableModels.DataItem;
import com.frf.frfrestaurant.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.frf.frfrestaurant.Core.MD5Password.getMD5EncryptedValue;

public class SignupActivity extends AppCompatActivity {

    private ExpandableListView parishList, villageList;
    private ArrayList<DataItem> parishArCategory, villageArCategory;
    private ArrayList<SubCategoryItem> parishArSubCategory, villageArSubCategory;
    private ArrayList<HashMap<String, String>> parishParentItems, villageParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> parishChildItems, villageChildItems;
    private ParishExpandableListAdapter parishExpandableListAdapter;
    private VillageExpandableListAdapter villageExpandableListAdapter;
    private JSONArray parishes, villages;
    private String selectedParish, selectedVillage;
    private String selectedParishName, selectedVillageName;
    double longitude, latitude;
    private int childHeight;
    private JSONObject lists, newParish, newVillage;

    private EditText restaurant_name, username, password, passwordConfirm, contact_person_f_name, contact_person_l_name,
            email, telephone_number, description, street;
    private TimePicker open_time, close_time;
    private Button signupButton;
    private TextView loginLink, addParishLink, addVillageLink;
    private ParseContent parseContent;
    private final int RegTask = 1;

    //for add modal
    private AlertDialog parishDialogBuilder, villageDialogBuilder;
    private EditText parishName, villageName;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        parseContent = new ParseContent(this);

        restaurant_name = findViewById(R.id.restaurant_name);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        passwordConfirm = findViewById(R.id.passwordConfirm);
        contact_person_f_name = findViewById(R.id.contact_person_f_name);
        contact_person_l_name = findViewById(R.id.contact_person_l_name);
        email = findViewById(R.id.email);
        telephone_number = findViewById(R.id.telephone_number);
        description = findViewById(R.id.description);
        street = findViewById(R.id.street);

        open_time = findViewById(R.id.open_time);
        open_time.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            open_time.setHour(0);
            open_time.setMinute(0);
        }
        else{
            open_time.setCurrentHour(0);
            open_time.setCurrentMinute(0);
        }

        close_time = findViewById(R.id.close_time);
        close_time.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            close_time.setHour(0);
            close_time.setMinute(0);
        }
        else{
            close_time.setCurrentHour(0);
            close_time.setCurrentMinute(0);
        }

        signupButton = findViewById(R.id.signupButton);
        loginLink = findViewById(R.id.loginLink);
        addParishLink = findViewById(R.id.addParishLink);
        addVillageLink = findViewById(R.id.addVillageLink);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    signup();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                SignupActivity.this.finish();
            }
        });

        addParishLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createParishModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        addVillageLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createVillageModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        new FetchListsAsyncTask().execute();

    }


    private class FetchListsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(SignupActivity.this)) {
                Toast.makeText(SignupActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(SignupActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.SIGNUPINDEX);
                response = req.prepare(HttpRequest.Method.POST).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("filtersjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }

    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            lists = parseContent.getDataObj(result);
        } else {//if something goes wrong with the retrieval, put them back at the login screen with an error toast
            Toast.makeText(SignupActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        Log.d("filters", String.valueOf(lists));

//VILLAGES
        villageList = findViewById(R.id.villageList);

        DataItem villageItem = new DataItem();
        villageItem.setCategoryId("village");
        villageItem.setCategoryName("Village");

        villageArCategory = new ArrayList<>();
        villageArSubCategory = new ArrayList<>();
        villageParentItems = new ArrayList<>();
        villageChildItems = new ArrayList<>();

        try {
            villages = lists.getJSONArray(AndyConstants.Params.VILLAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < villages.length(); i++) {
            JSONObject village = null;
            try {
                village = villages.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("village");
                subCategoryItem.setSubId(String.valueOf(village.getInt(AndyConstants.Params.VID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(village.getString(AndyConstants.Params.VILLAGE_NAME));
                villageArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        villageItem.setSubCategory(villageArSubCategory);
        villageArCategory.add(villageItem);

        for (DataItem data : villageArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            villageChildItems.add(childArrayList);
            villageParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = villageParentItems;
        ExpandableConstantManager.childItems = villageChildItems;

        Log.d("TAG", "village parent: " + villageParentItems);
        Log.d("TAG", "village children: " + villageChildItems);

        villageExpandableListAdapter = new VillageExpandableListAdapter(this, villageParentItems, villageChildItems, false);

        villageList.setAdapter(villageExpandableListAdapter);

        Log.d("TAG", "village parent: " + villageParentItems);
        Log.d("TAG", "village children: " + villageChildItems);

        villageList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("village children","count= "+villageList.getChildCount());
                for (int i = 0; i < villageList.getChildCount(); i++) {
                    childHeight = villageList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + villageList.getDividerHeight();
                }

                Log.d("count", String.valueOf(villageExpandableListAdapter.getChildrenCount(groupPosition)));
                villageList.getLayoutParams().height = (height) * (villageExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        villageList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                villageList.getLayoutParams().height = childHeight;
            }
        });

//PARISHES
        parishList = findViewById(R.id.parishList);
        Log.d("parishList", String.valueOf(parishList));

        DataItem parishItem = new DataItem();
        parishItem.setCategoryId("parish");
        parishItem.setCategoryName("Parish");

        parishArCategory = new ArrayList<>();
        parishArSubCategory = new ArrayList<>();
        parishParentItems = new ArrayList<>();
        parishChildItems = new ArrayList<>();

        try {
            parishes = lists.getJSONArray(AndyConstants.Params.PARISH);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < parishes.length(); i++) {
            JSONObject parish = null;
            try {
                parish = parishes.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("parish");
                subCategoryItem.setSubId(String.valueOf(parish.getInt(AndyConstants.Params.PID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(parish.getString(AndyConstants.Params.PARISH_NAME));
                parishArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        parishItem.setSubCategory(parishArSubCategory);
        parishArCategory.add(parishItem);

        for (DataItem data : parishArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            parishChildItems.add(childArrayList);
            parishParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = parishParentItems;
        ExpandableConstantManager.childItems = parishChildItems;

        Log.d("TAG", "parish parent: " + parishParentItems);
        Log.d("TAG", "parish children: " + parishChildItems);

        parishExpandableListAdapter = new ParishExpandableListAdapter(this, parishParentItems, parishChildItems, false);

        parishList.setAdapter(parishExpandableListAdapter);

        Log.d("TAG", "parish parent: " + parishParentItems);
        Log.d("TAG", "parish children: " + parishChildItems);

        parishList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("parish children","count= "+parishList.getChildCount());
                for (int i = 0; i < parishList.getChildCount(); i++) {
                    childHeight = parishList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + parishList.getDividerHeight();
                }

                Log.d("count", String.valueOf(parishExpandableListAdapter.getChildrenCount(groupPosition)));
                parishList.getLayoutParams().height = (height) * (parishExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        parishList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                parishList.getLayoutParams().height = childHeight;
            }
        });
    }



    public void createParishModal()throws IOException, JSONException {
        Log.d("here","inside create parish modal");
        parishDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Parish");
        parishName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parishDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addParish();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                parishDialogBuilder.dismiss();
            }
        });

        parishDialogBuilder.setView(dialogView);
        parishDialogBuilder.show();

    }


    public void addParish()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(SignupActivity.this)) {
            Toast.makeText(SignupActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(SignupActivity.this);
        final HashMap<String, String> parishMap = new HashMap<>();
        parishMap.put(AndyConstants.Params.PARISH_NAME,parishName.getText().toString());//get the parish name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PARISHESADD);
                    response = req.prepare(HttpRequest.Method.POST).withData(parishMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("parish res:", result);
                afterAddParish(result);
            }
        }.execute();
    }
    private void afterAddParish(String response) {
        Log.d("parishjson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
         if (parseContent.isSuccess(response)) {
             Toast.makeText(SignupActivity.this, "Parish added!", Toast.LENGTH_SHORT).show();
             parishDialogBuilder.dismiss();

             //repopulate the parish list and select the new value
             lists = parseContent.getDataObj(response);


//        arSubCategory = new ArrayList<>();
//

//PARISHES
             parishList = findViewById(R.id.parishList);
             Log.d("parishList", String.valueOf(parishList));

             DataItem parishItem = new DataItem();
             parishItem.setCategoryId("parish");
             parishItem.setCategoryName("Parish");

             parishArCategory = new ArrayList<>();
             parishArSubCategory = new ArrayList<>();
             parishParentItems = new ArrayList<>();
             parishChildItems = new ArrayList<>();

             try {
                 parishes = lists.getJSONArray(AndyConstants.Params.ALL);
                 newParish = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
             } catch (JSONException e) {
                 e.printStackTrace();
             }

             for (int i = 0; i < parishes.length(); i++) {
                 JSONObject parish = null;
                 try {
                     parish = parishes.getJSONObject(i);
                     SubCategoryItem subCategoryItem = new SubCategoryItem();
                     subCategoryItem.setCategoryId("parish");
                     subCategoryItem.setSubId(String.valueOf(parish.getInt(AndyConstants.Params.PID)));

                     if(newParish.getInt(AndyConstants.Params.PID) == parish.getInt(AndyConstants.Params.PID)){//if the parish is the new one
                         subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                     }
                     else{
                         subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                     }

                     subCategoryItem.setSubCategoryName(parish.getString(AndyConstants.Params.PARISH_NAME));
                     parishArSubCategory.add(subCategoryItem);
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
             }

             parishItem.setSubCategory(parishArSubCategory);
             parishArCategory.add(parishItem);

             for (DataItem data : parishArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                 ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                 HashMap<String, String> mapParent = new HashMap<String, String>();

                 mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                 mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                 int countIsChecked = 0;
                 for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                     HashMap<String, String> mapChild = new HashMap<String, String>();
                     mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                     mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                     mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                     mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                     if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                         countIsChecked++;
                     }
                     childArrayList.add(mapChild);
                 }

                 if (countIsChecked == data.getSubCategory().size()) {

                     data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                 } else {
                     data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                 }

                 mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                 parishChildItems.add(childArrayList);
                 parishParentItems.add(mapParent);

             }

             ExpandableConstantManager.parentItems = parishParentItems;
             ExpandableConstantManager.childItems = parishChildItems;

             Log.d("TAG", "parish parent: " + parishParentItems);
             Log.d("TAG", "parish children: " + parishChildItems);

             parishExpandableListAdapter = new ParishExpandableListAdapter(this, parishParentItems, parishChildItems, false);

             parishList.setAdapter(parishExpandableListAdapter);

             Log.d("TAG", "parish parent: " + parishParentItems);
             Log.d("TAG", "parish children: " + parishChildItems);

             parishList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                 @Override
                 public void onGroupExpand(int groupPosition) {
                     int height = 0;
                     Log.d("parish children","count= "+parishList.getChildCount());
                     for (int i = 0; i < parishList.getChildCount(); i++) {
                         childHeight = parishList.getChildAt(i).getMeasuredHeight();
                         height = childHeight + parishList.getDividerHeight();
                     }

                     Log.d("count", String.valueOf(parishExpandableListAdapter.getChildrenCount(groupPosition)));
                     parishList.getLayoutParams().height = (height) * (parishExpandableListAdapter.getChildrenCount(groupPosition)+1);
                 }
             });

             // Listview Group collapsed listener
             parishList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                 @Override
                 public void onGroupCollapse(int groupPosition) {
                     parishList.getLayoutParams().height = childHeight;
                 }
             });

         }else {
             Toast.makeText(SignupActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
         }
    }



    public void createVillageModal()throws IOException, JSONException {
        Log.d("here","inside create village modal");
        villageDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Village");
        villageName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                villageDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addVillage();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                villageDialogBuilder.dismiss();
            }
        });

        villageDialogBuilder.setView(dialogView);
        villageDialogBuilder.show();

    }


    public void addVillage()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(SignupActivity.this)) {
            Toast.makeText(SignupActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(SignupActivity.this);
        final HashMap<String, String> villageMap = new HashMap<>();
        villageMap.put(AndyConstants.Params.VILLAGE_NAME,villageName.getText().toString());//get the village name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.VILLAGESADD);
                    response = req.prepare(HttpRequest.Method.POST).withData(villageMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("village res:", result);
                afterAddVillage(result);
            }
        }.execute();
    }
    private void afterAddVillage(String response) {
        Log.d("villagejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        if (parseContent.isSuccess(response)) {
            Toast.makeText(SignupActivity.this, "Village added!", Toast.LENGTH_SHORT).show();
            villageDialogBuilder.dismiss();

            //repopulate the village list and select the new value
            lists = parseContent.getDataObj(response);

//VILLAGES
            villageList = findViewById(R.id.villageList);
            Log.d("villageList", String.valueOf(villageList));

            DataItem villageItem = new DataItem();
            villageItem.setCategoryId("village");
            villageItem.setCategoryName("Village");

            villageArCategory = new ArrayList<>();
            villageArSubCategory = new ArrayList<>();
            villageParentItems = new ArrayList<>();
            villageChildItems = new ArrayList<>();

            try {
                villages = lists.getJSONArray(AndyConstants.Params.ALL);
                newVillage = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < villages.length(); i++) {
                JSONObject village = null;
                try {
                    village = villages.getJSONObject(i);
                    SubCategoryItem subCategoryItem = new SubCategoryItem();
                    subCategoryItem.setCategoryId("village");
                    subCategoryItem.setSubId(String.valueOf(village.getInt(AndyConstants.Params.VID)));

                    if(newVillage.getInt(AndyConstants.Params.VID) == village.getInt(AndyConstants.Params.VID)){//if the village is the new one
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    subCategoryItem.setSubCategoryName(village.getString(AndyConstants.Params.VILLAGE_NAME));
                    villageArSubCategory.add(subCategoryItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            villageItem.setSubCategory(villageArSubCategory);
            villageArCategory.add(villageItem);

            for (DataItem data : villageArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> mapParent = new HashMap<String, String>();

                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                int countIsChecked = 0;
                for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                    HashMap<String, String> mapChild = new HashMap<String, String>();
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                    mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                    mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                    if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                        countIsChecked++;
                    }
                    childArrayList.add(mapChild);
                }

                if (countIsChecked == data.getSubCategory().size()) {

                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                } else {
                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                villageChildItems.add(childArrayList);
                villageParentItems.add(mapParent);

            }

            ExpandableConstantManager.parentItems = villageParentItems;
            ExpandableConstantManager.childItems = villageChildItems;

            Log.d("TAG", "village parent: " + villageParentItems);
            Log.d("TAG", "village children: " + villageChildItems);

            villageExpandableListAdapter = new VillageExpandableListAdapter(this, villageParentItems, villageChildItems, false);

            villageList.setAdapter(villageExpandableListAdapter);

            Log.d("TAG", "village parent: " + villageParentItems);
            Log.d("TAG", "village children: " + villageChildItems);

            villageList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int height = 0;
                    Log.d("village children","count= "+villageList.getChildCount());
                    for (int i = 0; i < villageList.getChildCount(); i++) {
                        childHeight = villageList.getChildAt(i).getMeasuredHeight();
                        height = childHeight + villageList.getDividerHeight();
                    }

                    Log.d("count", String.valueOf(villageExpandableListAdapter.getChildrenCount(groupPosition)));
                    villageList.getLayoutParams().height = (height) * (villageExpandableListAdapter.getChildrenCount(groupPosition)+1);
                }
            });

            // Listview Group collapsed listener
            villageList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                    villageList.getLayoutParams().height = childHeight;
                }
            });

        }else {
            Toast.makeText(SignupActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }





    public void signup()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(SignupActivity.this)) {
            Toast.makeText(SignupActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(SignupActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.RESTAURANT_NAME,restaurant_name.getText().toString());
        map.put(AndyConstants.Params.USERNAME,username.getText().toString());

        String encryptedPassword = getMD5EncryptedValue(password.getText().toString());
        map.put(AndyConstants.Params.PASSWORD, encryptedPassword);

        String encryptedPasswordConfirm = getMD5EncryptedValue(passwordConfirm.getText().toString());
        map.put(AndyConstants.Params.PASSWORDCONFIRM, encryptedPasswordConfirm);

        map.put(AndyConstants.Params.CONTACT_PERSON_F_NAME,contact_person_f_name.getText().toString());
        map.put(AndyConstants.Params.CONTACT_PERSON_L_NAME,contact_person_l_name.getText().toString());
        map.put(AndyConstants.Params.EMAIL,email.getText().toString());
        map.put(AndyConstants.Params.TELEPHONE_NUMBER,telephone_number.getText().toString());
        map.put(AndyConstants.Params.DESCRIPTION,description.getText().toString());
        map.put(AndyConstants.Params.STREET,street.getText().toString());


        //selectedOpenTime
        int open_hour, open_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            open_hour = open_time.getHour();
            open_minute = open_time.getMinute();
        }
        else{
            open_hour = open_time.getCurrentHour();
            open_minute = open_time.getCurrentMinute();
        }

        String selectedOpenTime;
        String desiredOpenTime = open_hour +":"+ open_minute;

        if(desiredOpenTime.equals("00:00") || desiredOpenTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedOpenTime = null;
        }
        else{
            selectedOpenTime = desiredOpenTime;
        }
        map.put(AndyConstants.Params.OPEN_TIME,selectedOpenTime);


        //selectedCloseTime
        int close_hour, close_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            close_hour = close_time.getHour();
            close_minute = close_time.getMinute();
        }
        else{
            close_hour = close_time.getCurrentHour();
            close_minute = close_time.getCurrentMinute();
        }

        String selectedCloseTime;
        String desiredCloseTime = close_hour +":"+ close_minute;
        if(desiredCloseTime.equals("00:00") || desiredCloseTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedCloseTime = null;
        }
        else{
            selectedCloseTime = desiredCloseTime;
        }
        map.put(AndyConstants.Params.CLOSE_TIME,selectedCloseTime);

        //selectedParish
        for (int i = 0; i < ParishExpandableListAdapter.parentItems.size(); i++ ) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < ParishExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                    selectedParishName = ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME);
                    Log.d("selectedParishName",selectedParishName);
                }

            }
            selectedParish = String.valueOf(children);
            Log.d("selectedParish",selectedParish);
        }
        map.put(AndyConstants.Params.PARISH,selectedParish);

        //selectedVillage
        for (int i = 0; i < VillageExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < VillageExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                    selectedVillageName = VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME);
                    Log.d("selectedVillageName",selectedVillageName);
                }
            }
            selectedVillage = String.valueOf(children);
            Log.d("selectedVillage",selectedVillage);
        }
        map.put(AndyConstants.Params.VILLAGE,selectedVillage);


        //geocode the street, village and parish to get the longitude and latitude
        String addr = street.getText() + ", " + selectedVillageName + ", " + selectedParishName + ", Barbados";

        Geocoder gc = new Geocoder(this);
        if(gc.isPresent()){
            List<Address> list = null;
            try {
                list = gc.getFromLocationName(addr, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = list.get(0);
            latitude = address.getLatitude();
            longitude = address.getLongitude();
        }

        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(longitude));
        map.put(AndyConstants.Params.LATITUDE, String.valueOf(latitude));


        Log.d("request: ", String.valueOf(map));


        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.SIGNUPACTION);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result, RegTask);
            }
        }.execute();
    }
    private void onTaskCompleted(String response,int task) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:

                if (parseContent.isSuccess(response)) {
                    Toast.makeText(SignupActivity.this, "Registered Successfully! Please Login.", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    this.finish();
                }else {
                    Toast.makeText(SignupActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }
}
