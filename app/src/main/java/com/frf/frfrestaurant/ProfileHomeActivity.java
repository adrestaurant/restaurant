package com.frf.frfrestaurant;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ProfileHomeActivity extends AppCompatActivity {

    private Button addressButton, contactButton, detailsButton, usernameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_home);

        addressButton = findViewById(R.id.addressButton);
        contactButton = findViewById(R.id.contactButton);
        detailsButton = findViewById(R.id.detailsButton);
        usernameButton = findViewById(R.id.usernameButton);

        addressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileHomeActivity.this, ProfileAddressHomeActivity.class);
                startActivity(intent);
            }
        });

        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileHomeActivity.this, ProfileContactActivity.class);
                startActivity(intent);
            }
        });

        detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileHomeActivity.this, ProfileDetailsActivity.class);
                startActivity(intent);
            }
        });

        usernameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileHomeActivity.this, ProfileUsernameActivity.class);
                startActivity(intent);
            }
        });
    }



}
