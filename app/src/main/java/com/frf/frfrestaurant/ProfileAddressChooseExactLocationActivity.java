package com.frf.frfrestaurant;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class ProfileAddressChooseExactLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView address;
    private Button updateButton;
    public static final int MAP_PIN_LOCATION_REQUEST_CODE = 99;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    private JSONObject current;
    private LatLng currentLatLng;
    private double currentLat, currentLng;
    private String addr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_address_choose_precise_location);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        address = findViewById(R.id.address);

        updateButton = findViewById(R.id.updateButton);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    update();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //check permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MAP_PIN_LOCATION_REQUEST_CODE);
                return;
            }
        }

        new FetchLocationAsyncTask().execute();

//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
    }


    private class FetchLocationAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ProfileAddressChooseExactLocationActivity.this)) {
                Toast.makeText(ProfileAddressChooseExactLocationActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ProfileAddressChooseExactLocationActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILECOORDINATESINDEX);
                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                Log.d("response", response);
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("detailsson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }


    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            current = parseContent.getDataObj(result);


            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);


        } else {//if something goes wrong with the retrieval, put them back at the profile address home screen with an error toast
            Toast.makeText(ProfileAddressChooseExactLocationActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ProfileAddressChooseExactLocationActivity.this, ProfileAddressHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        Log.d("results", String.valueOf(current));

        try {
            currentLat = current.getDouble(AndyConstants.Params.LATITUDE);
            currentLng = current.getDouble(AndyConstants.Params.LONGITUDE);
            addr = "Address: "+current.getString(AndyConstants.Params.STREET) + ", " + current.getString(AndyConstants.Params.VILLAGE) + ", " + current.getString(AndyConstants.Params.PARISH) + ", Barbados";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        address.setText(addr);
        currentLatLng = new LatLng(currentLat, currentLng);

        Log.d("currentLat", String.valueOf(currentLat));
        Log.d("currentLng", String.valueOf(currentLng));
        Log.d("currenLattLng", String.valueOf(currentLatLng));

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);

        // Add a marker at current coordinates location and move the camera
        mMap.addMarker(new MarkerOptions().position(currentLatLng).title("Current Coordinates").snippet("Current Latitude:"+currentLat+"\nCurrent Longitude: "+currentLng).draggable(false)).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,15));

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(ProfileAddressChooseExactLocationActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(ProfileAddressChooseExactLocationActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(ProfileAddressChooseExactLocationActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();

                currentLat = latLng.latitude;
                currentLng = latLng.longitude;

                mMap.addMarker(new MarkerOptions().position(latLng).title("New Coordinates").snippet("New Latitude:"+currentLat+"\nCurrent Longitude: "+currentLng).draggable(false)).showInfoWindow();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                Log.d("newlatLng", String.valueOf(latLng));
                Log.d("newlat", String.valueOf(currentLat));
                Log.d("newlng", String.valueOf(currentLat));
            }
        });


    }

    public void update()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(ProfileAddressChooseExactLocationActivity.this)) {
            Toast.makeText(ProfileAddressChooseExactLocationActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileAddressChooseExactLocationActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.LATITUDE, String.valueOf(currentLat));
        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(currentLng));

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILECOORDINATESUPDATE);
                    map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();

                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result);
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {
            Toast.makeText(ProfileAddressChooseExactLocationActivity.this, "Location Successfully Updated!", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(ProfileAddressChooseExactLocationActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }


}
