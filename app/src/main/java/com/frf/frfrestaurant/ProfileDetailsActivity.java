package com.frf.frfrestaurant;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import static com.frf.frfrestaurant.Core.MD5Password.getMD5EncryptedValue;

public class ProfileDetailsActivity extends AppCompatActivity {

    private EditText restaurant_name, description, password;
    private TimePicker open_time, close_time;
    private Button updateButton;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    private JSONArray details;
    private String oldPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        restaurant_name = findViewById(R.id.restaurant_name);
        description = findViewById(R.id.description);
        password = findViewById(R.id.password);

        open_time = findViewById(R.id.open_time);
        open_time.setIs24HourView(true);
        close_time = findViewById(R.id.close_time);
        close_time.setIs24HourView(true);

        updateButton = findViewById(R.id.updateButton);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    update();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        new FetchDetailsAsyncTask().execute();

    }


    private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ProfileDetailsActivity.this)) {
                Toast.makeText(ProfileDetailsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ProfileDetailsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEDETAILSINDEX);

                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();

            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("responsejson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            if (parseContent.isSuccess(result)) {
                details = parseContent.getData((result));
                JSONObject dets = null;
                try {
                    dets = details.getJSONObject(0);

                    restaurant_name.setText(Html.fromHtml(dets.getString(AndyConstants.Params.RESTAURANT_NAME)));
                    description.setText(Html.fromHtml(dets.getString(AndyConstants.Params.DESCRIPTION)));
                    oldPassword = dets.getString(AndyConstants.Params.PASSWORD);

                    String otString = dets.getString(AndyConstants.Params.OPEN_TIME);
                    String ctString = dets.getString(AndyConstants.Params.CLOSE_TIME);

                    String[] otArray = otString.split(":", 0);
                    String otHour = otArray[0];
                    String otMinute = otArray[1];

                    String[] ctArray = ctString.split(":", 0);
                    String ctHour = ctArray[0];
                    String ctMinute = ctArray[1];

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        open_time.setHour(Integer.parseInt(otHour));
                        open_time.setMinute(Integer.parseInt(otMinute));
                        close_time.setHour(Integer.parseInt(ctHour));
                        close_time.setMinute(Integer.parseInt(ctMinute));
                    }
                    else{
                        open_time.setCurrentHour(Integer.valueOf(otHour));
                        open_time.setCurrentMinute(Integer.valueOf(otMinute));
                        close_time.setCurrentHour(Integer.valueOf(ctHour));
                        close_time.setCurrentMinute(Integer.valueOf(ctMinute));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {//if something goes wrong with the retrieval, put them back at the login screen with an error toast
                Toast.makeText(ProfileDetailsActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileDetailsActivity.this, ProfileHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                ProfileDetailsActivity.this.finish();
            }
            Log.d("result", String.valueOf(result));
        }
    }

    public void update()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(ProfileDetailsActivity.this)) {
            Toast.makeText(ProfileDetailsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileDetailsActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.RESTAURANT_NAME, restaurant_name.getText().toString());
        map.put(AndyConstants.Params.DESCRIPTION, description.getText().toString());

        //deal with password
        String pwd = password.getText().toString();
        String encryptedPassword;
        if(pwd.isEmpty()){//if they didn't enter a password use the original encrypted password that's stored in preference helper
            encryptedPassword = preferenceHelper.getPassword();
        }
        else{
            encryptedPassword  = getMD5EncryptedValue(password.getText().toString());
        }

        map.put(AndyConstants.Params.PASSWORD, encryptedPassword);

        //selectedOpenTime
        int open_hour, open_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            open_hour = open_time.getHour();
            open_minute = open_time.getMinute();
        }
        else{
            open_hour = open_time.getCurrentHour();
            open_minute = open_time.getCurrentMinute();
        }

        String selectedOpenTime;
        String desiredOpenTime = open_hour +":"+ open_minute;

        if(desiredOpenTime.equals("00:00") || desiredOpenTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedOpenTime = null;
        }
        else{
            selectedOpenTime = desiredOpenTime;
        }
        map.put(AndyConstants.Params.OPEN_TIME,selectedOpenTime);


        //selectedCloseTime
        int close_hour, close_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            close_hour = close_time.getHour();
            close_minute = close_time.getMinute();
        }
        else{
            close_hour = close_time.getCurrentHour();
            close_minute = close_time.getCurrentMinute();
        }

        String selectedCloseTime;
        String desiredCloseTime = close_hour +":"+ close_minute;
        if(desiredCloseTime.equals("00:00") || desiredCloseTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedCloseTime = null;
        }
        else{
            selectedCloseTime = desiredCloseTime;
        }
        map.put(AndyConstants.Params.CLOSE_TIME,selectedCloseTime);


        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEDETAILSUPDATE);

                    map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result);
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {

            JSONArray newPass = parseContent.getData(response);
            JSONObject newPwd = null;
            try {
                newPwd = newPass.getJSONObject(0);
                String newPassword =  newPwd.getString(AndyConstants.Params.PASSWORD);

                //if the password was updated
                if(!newPassword.equals(preferenceHelper.getPassword())){
                    Toast.makeText(ProfileDetailsActivity.this, "Update Successful! Please login with new password.", Toast.LENGTH_SHORT).show();

                    preferenceHelper.clearPrefs();//clear all preferences (log them out)

                    Intent intent = new Intent(ProfileDetailsActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    this.finish();
                }
                else{//if the password wasn't changed
                    Toast.makeText(ProfileDetailsActivity.this, "Update Successful!", Toast.LENGTH_SHORT).show();

                    new FetchDetailsAsyncTask().execute();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {
            Toast.makeText(ProfileDetailsActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }

}
