package com.frf.frfrestaurant;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.AsyncTaskCompleteListener;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.MultiPartRequester;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;
import com.frf.frfrestaurant.ExpandableAdapters.MealExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableModels.DataItem;
import com.frf.frfrestaurant.ExpandableModels.SubCategoryItem;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DetailsMenuActivity extends AppCompatActivity implements AsyncTaskCompleteListener {

        private ExpandableListView mealList;
        private ArrayList<DataItem> mealArCategory;
        private ArrayList<SubCategoryItem> mealArSubCategory;
        private ArrayList<HashMap<String, String>> mealParentItems;
        private ArrayList<ArrayList<HashMap<String, String>>> mealChildItems;
        private MealExpandableListAdapter mealExpandableListAdapter;
        private JSONArray meals;
        private String selectedMeal;
        private int childHeight;
        private JSONObject lists;
        private JSONObject newMeal;

        private TimePicker start_time, end_time;
        private ImageView image;
        private Button chooseButton, updateButton;
        private TextView addMealLink;

        private final int GALLERY = 1;

        //storage permission code
        private static final int STORAGE_PERMISSION_CODE = 123;

        private int PICK_IMAGE_REQUEST = 1;
        private Bitmap bitmap;
        private Uri path;

        private PreferenceHelper preferenceHelper;
        private ParseContent parseContent;

        //for add modal
        private AlertDialog mealDialogBuilder;
        private EditText mealName;

        private String menu_id;
        private JSONObject details, current_info;

    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_details_menu);

            //Requesting storage permission
            requestStoragePermission();

            Intent intent = getIntent();
            menu_id =  intent.getStringExtra(AndyConstants.Params.MENU_ID);
            Log.d("det men menu_id",menu_id);

            preferenceHelper = new PreferenceHelper(this);
            parseContent = new ParseContent(this);

            image = findViewById(R.id.image);

            start_time = findViewById(R.id.start_time);
            start_time.setIs24HourView(true);

            end_time = findViewById(R.id.end_time);
            end_time.setIs24HourView(true);

            addMealLink = findViewById(R.id.addMealLink);
            chooseButton = findViewById(R.id.chooseButton);
            updateButton = findViewById(R.id.updateButton);

            addMealLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        createMealModal();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            chooseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFileChooser();
                }
            });

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateMenu();
                }
            });

            new FetchDetailsAsyncTask().execute();

    }

        private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (!AndyUtils.isNetworkAvailable(DetailsMenuActivity.this)) {
                    Toast.makeText(DetailsMenuActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Display progress bar
                AndyUtils.showSimpleProgressDialog(DetailsMenuActivity.this);
            }

            @Override
            protected String doInBackground(String... params) {

                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.MENUSDETAILS);
                    final HashMap<String, String> map = new HashMap<>();
                    map.put(AndyConstants.Params.RID, preferenceHelper.getRID());
                    map.put(AndyConstants.Params.MENU_ID,menu_id);

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=parseContent.getErrorMessage(response);
                }
                return response;
            }

            protected void onPostExecute(String result) {
                Log.d("filtersjson", result);
                AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
                setupReferences(result);
            }

        }

        private void setupReferences(String result) {

            Log.d("received refs", result);
            if (parseContent.isSuccess(result)) {
                details = parseContent.getDataObj(result);
                Log.d("details", String.valueOf(details));

                try {
                    meals = details.getJSONArray("all_meals");
                    Log.d("meals", String.valueOf(meals));
                    current_info = details.getJSONObject("current_info");
                    Log.d("current_info", String.valueOf(current_info));
                    //current info

                    //times
                    String stString = current_info.getString(AndyConstants.Params.START_TIME);
                    String etString = current_info.getString(AndyConstants.Params.END_TIME);

                    String[] stArray = stString.split(":", 0);
                    String stHour = stArray[0];
                    String stMinute = stArray[1];

                    String[] etArray = etString.split(":", 0);
                    String etHour = etArray[0];
                    String etMinute = etArray[1];

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        start_time.setHour(Integer.parseInt(stHour));
                        start_time.setMinute(Integer.parseInt(stMinute));
                        end_time.setHour(Integer.parseInt(etHour));
                        end_time.setMinute(Integer.parseInt(etMinute));
                    }
                    else{
                        start_time.setCurrentHour(Integer.valueOf(stHour));
                        start_time.setCurrentMinute(Integer.valueOf(stMinute));
                        end_time.setCurrentHour(Integer.valueOf(etHour));
                        end_time.setCurrentMinute(Integer.valueOf(etMinute));
                    }

                    //image
                    Picasso.get().load(Uri.parse(AndyConstants.ServiceType.IMAGES + current_info.getString("menu_image")))
                            .resize(600, 300).placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder_error)
                            .into(image);


                    //Meals
                    mealList = findViewById(R.id.mealList);

                    DataItem mealItem = new DataItem();
                    mealItem.setCategoryId("meal");
                    mealItem.setCategoryName("Meal");

                    mealArCategory = new ArrayList<>();
                    mealArSubCategory = new ArrayList<>();
                    mealParentItems = new ArrayList<>();
                    mealChildItems = new ArrayList<>();

                    for (int i = 0; i < meals.length(); i++) {
                        JSONObject meal = null;
                        try {
                            meal = meals.getJSONObject(i);
                            Log.d("meal", String.valueOf(meal));
                            SubCategoryItem subCategoryItem = new SubCategoryItem();
                            subCategoryItem.setCategoryId("meal");
                            subCategoryItem.setSubId(String.valueOf(meal.getInt(AndyConstants.Params.MEAL_ID)));

                            if(current_info.getInt(AndyConstants.Params.MEAL_ID) == meal.getInt(AndyConstants.Params.MEAL_ID)) {//if this is the current meal
                                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                            }
                            else{
                                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                            }

                            subCategoryItem.setSubCategoryName(meal.getString(AndyConstants.Params.DB_MEAL_NAME));
                            mealArSubCategory.add(subCategoryItem);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    mealItem.setSubCategory(mealArSubCategory);
                    mealArCategory.add(mealItem);

                    for (DataItem data : mealArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                        ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                        HashMap<String, String> mapParent = new HashMap<String, String>();

                        mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                        mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                        int countIsChecked = 0;
                        for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                            HashMap<String, String> mapChild = new HashMap<String, String>();
                            mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                            mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                            mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                            mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                            if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                                countIsChecked++;
                            }
                            childArrayList.add(mapChild);
                        }

                        if (countIsChecked == data.getSubCategory().size()) {

                            data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                        } else {
                            data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                        }

                        mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                        mealChildItems.add(childArrayList);
                        mealParentItems.add(mapParent);

                    }

                    ExpandableConstantManager.parentItems = mealParentItems;
                    ExpandableConstantManager.childItems = mealChildItems;

                    Log.d("TAG", "meal parent: " + mealParentItems);
                    Log.d("TAG", "meal children: " + mealChildItems);

                    mealExpandableListAdapter = new MealExpandableListAdapter(this, mealParentItems, mealChildItems, false);

                    mealList.setAdapter(mealExpandableListAdapter);

                    Log.d("TAG", "meal parent: " + mealParentItems);
                    Log.d("TAG", "meal children: " + mealChildItems);

                    mealList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                        @Override
                        public void onGroupExpand(int groupPosition) {
                            int height = 0;
                            for (int i = 0; i < mealList.getChildCount(); i++) {
                                childHeight = mealList.getChildAt(i).getMeasuredHeight();
                                height = childHeight + mealList.getDividerHeight();
                            }
                            mealList.getLayoutParams().height = height * (mealExpandableListAdapter.getChildrenCount(groupPosition)+1);
                        }
                    });

                    // Listview Group collapsed listener
                    mealList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                        @Override
                        public void onGroupCollapse(int groupPosition) {
                            mealList.getLayoutParams().height = childHeight;
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {//if something goes wrong with the retrieval, put them back at the listing screen with an error toast
                Toast.makeText(DetailsMenuActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(DetailsMenuActivity.this, DetailsMenuHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(AndyConstants.Params.MENU_ID, menu_id);
                startActivity(intent);
                this.finish();
            }
        }

        public void createMealModal()throws IOException, JSONException {
            Log.d("here","inside create meal modal");
            mealDialogBuilder = new AlertDialog.Builder(this).create();
            // Get the layout inflater
            LayoutInflater inflater = this.getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

            TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
            modalTitle.setText("Add Meal");
            mealName = dialogView.findViewById(R.id.name);
            Button submitButton = dialogView.findViewById(R.id.submitButton);
            Button cancelButton = dialogView.findViewById(R.id.cancelButton);


            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mealDialogBuilder.dismiss();
                }
            });
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        addMeal();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mealDialogBuilder.dismiss();
                }
            });

            mealDialogBuilder.setView(dialogView);
            mealDialogBuilder.show();

        }


        public void addMeal()throws IOException, JSONException {
            if (!AndyUtils.isNetworkAvailable(DetailsMenuActivity.this)) {
                Toast.makeText(DetailsMenuActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            AndyUtils.showSimpleProgressDialog(DetailsMenuActivity.this);
            final HashMap<String, String> mealMap = new HashMap<>();
            mealMap.put(AndyConstants.Params.DB_MEAL_NAME,mealName.getText().toString());//get the meal name they put in

            new AsyncTask<Void, Void, String>(){
                protected String doInBackground(Void[] params) {
                    String response="";
                    try {
                        HttpRequest req = new HttpRequest(AndyConstants.ServiceType.MEALSADD);

                        mealMap.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                        response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(mealMap).sendAndReadString();
                    } catch (Exception e) {
                        response=e.getMessage();
                    }
                    return response;
                }
                protected void onPostExecute(String result) {
                    //do something with response
                    Log.d("meal res:", result);
                    afterAddMeal(result);
                }
            }.execute();
        }
        private void afterAddMeal(String response) {
            Log.d("mealjson", response.toString());
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            if (parseContent.isSuccess(response)) {
                Toast.makeText(DetailsMenuActivity.this, "Meal added!", Toast.LENGTH_SHORT).show();
                mealDialogBuilder.dismiss();

                //repopulate the meal list and select the new value
                lists = parseContent.getDataObj(response);


//        arSubCategory = new ArrayList<>();
//

//MEALS
                mealList = findViewById(R.id.mealList);
                Log.d("mealList", String.valueOf(mealList));

                DataItem mealItem = new DataItem();
                mealItem.setCategoryId("meal");
                mealItem.setCategoryName("Meal");

                mealArCategory = new ArrayList<>();
                mealArSubCategory = new ArrayList<>();
                mealParentItems = new ArrayList<>();
                mealChildItems = new ArrayList<>();

                try {
                    meals = lists.getJSONArray(AndyConstants.Params.ALL);
                    newMeal = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < meals.length(); i++) {
                    JSONObject meal = null;
                    try {
                        meal = meals.getJSONObject(i);
                        SubCategoryItem subCategoryItem = new SubCategoryItem();
                        subCategoryItem.setCategoryId("meal");
                        subCategoryItem.setSubId(String.valueOf(meal.getInt(AndyConstants.Params.MEAL_ID)));

                        if(newMeal.getInt(AndyConstants.Params.MEAL_ID) == meal.getInt(AndyConstants.Params.MEAL_ID)){//if the meal is the new one
                            subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                        }
                        else{
                            subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                        }

                        subCategoryItem.setSubCategoryName(meal.getString(AndyConstants.Params.DB_MEAL_NAME));
                        mealArSubCategory.add(subCategoryItem);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                mealItem.setSubCategory(mealArSubCategory);
                mealArCategory.add(mealItem);

                for (DataItem data : mealArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                    ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> mapParent = new HashMap<String, String>();

                    mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                    mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                    int countIsChecked = 0;
                    for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                        HashMap<String, String> mapChild = new HashMap<String, String>();
                        mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                        mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                        mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                        mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                        if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                            countIsChecked++;
                        }
                        childArrayList.add(mapChild);
                    }

                    if (countIsChecked == data.getSubCategory().size()) {

                        data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    } else {
                        data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                    mealChildItems.add(childArrayList);
                    mealParentItems.add(mapParent);

                }

                ExpandableConstantManager.parentItems = mealParentItems;
                ExpandableConstantManager.childItems = mealChildItems;

                Log.d("TAG", "meal parent: " + mealParentItems);
                Log.d("TAG", "meal children: " + mealChildItems);

                mealExpandableListAdapter = new MealExpandableListAdapter(this, mealParentItems, mealChildItems, false);

                mealList.setAdapter(mealExpandableListAdapter);

                Log.d("TAG", "meal parent: " + mealParentItems);
                Log.d("TAG", "meal children: " + mealChildItems);

                mealList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    @Override
                    public void onGroupExpand(int groupPosition) {
                        int height = 0;
                        for (int i = 0; i < mealList.getChildCount(); i++) {
                            childHeight = mealList.getChildAt(i).getMeasuredHeight();
                            height = childHeight + mealList.getDividerHeight();
                        }
                        mealList.getLayoutParams().height = height * (mealExpandableListAdapter.getChildrenCount(groupPosition)+1);
                    }
                });

                // Listview Group collapsed listener
                mealList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                    @Override
                    public void onGroupCollapse(int groupPosition) {
                        mealList.getLayoutParams().height = childHeight;
                    }
                });

            }else {
                Toast.makeText(DetailsMenuActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
            }
        }


        //method to show file chooser
        private void showFileChooser() {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        }

        //handling the image chooser activity result
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                path = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                    image.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //method to get the file path from uri
        public String getPath(Uri uri) {
            String path = null;

            if(uri != null) {
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                cursor.moveToFirst();
                String document_id = cursor.getString(0);
                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                cursor.close();

                cursor = getContentResolver().query(
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
                cursor.moveToFirst();
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                cursor.close();
            }

            return path;
        }

        //Requesting permission
        private void requestStoragePermission() {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                return;

            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }


        //This method will be called when the user will tap on allow or deny
        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

            //Checking the request code of our request
            if (requestCode == STORAGE_PERMISSION_CODE) {

                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Displaying a toast
                    Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
                } else {
                    //Displaying another toast if permission is not granted
                    Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
                }
            }
        }




        public void updateMenu(){
            if (!AndyUtils.isNetworkAvailable(DetailsMenuActivity.this)) {
                Toast.makeText(DetailsMenuActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("Authorization","Basic "+preferenceHelper.getUserPass());
            map.put("url", AndyConstants.ServiceType.MENUSUPDATE);
            map.put(AndyConstants.Params.RID,preferenceHelper.getRID());
            map.put(AndyConstants.Params.MENU_ID,menu_id);

            if(path == null){//if they didn't choose a new image pass back the path to the old one (on server)
                try {
                    map.put("image_url", current_info.getString("menu_image"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {//if they set a new image get the path to it
                map.put("image", getPath(path));
            }

            //get the selected values
            //selectedMeal
            for (int i = 0; i < MealExpandableListAdapter.parentItems.size(); i++) {
                ArrayList<Integer> children = new ArrayList<Integer>();

                for (int j = 0; j < MealExpandableListAdapter.childItems.get(i).size(); j++) {

                    String isChildChecked = MealExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                    Integer childID = Integer.parseInt(MealExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                    if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        children.add(childID);
                    }
                }
                selectedMeal = String.valueOf(children);
                Log.d("selectedMeal",selectedMeal);
            }
            map.put(AndyConstants.Params.MEAL_ID,selectedMeal);

            //selectedStartTime
            int start_hour, start_minute;

            if (Build.VERSION.SDK_INT >= 23 ){
                start_hour = start_time.getHour();
                start_minute = start_time.getMinute();
            }
            else{
                start_hour = start_time.getCurrentHour();
                start_minute = start_time.getCurrentMinute();
            }

            String selectedStartTime;
            String desiredStartTime = start_hour +":"+ start_minute;

            if(desiredStartTime.equals("00:00") || desiredStartTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
                selectedStartTime = null;
            }
            else{
                selectedStartTime = desiredStartTime;
            }
            map.put(AndyConstants.Params.START_TIME,selectedStartTime);

            //selectedEndTime
            int end_hour, end_minute;

            if (Build.VERSION.SDK_INT >= 23 ){
                end_hour = end_time.getHour();
                end_minute = end_time.getMinute();
            }
            else{
                end_hour = end_time.getCurrentHour();
                end_minute = end_time.getCurrentMinute();
            }

            String selectedEndTime;
            String desiredEndTime = end_hour +":"+ end_minute;
            if(desiredEndTime.equals("00:00") || desiredEndTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
                selectedEndTime = null;
            }
            else{
                selectedEndTime = desiredEndTime;
            }
            map.put(AndyConstants.Params.END_TIME,selectedEndTime);
            new MultiPartRequester(DetailsMenuActivity.this, map, GALLERY,  DetailsMenuActivity.this);
            AndyUtils.showSimpleProgressDialog(this);


        }

        @Override
        public void onTaskCompleted(String response, int serviceCode) {
            AndyUtils.removeSimpleProgressDialog();
            Log.d("res", response.toString());

            if (parseContent.isSuccess(response)) {
                Toast.makeText(DetailsMenuActivity.this, "Menu successfully updated!", Toast.LENGTH_SHORT).show();
                try {
                    menu_id = parseContent.getDataObj(response).getString(AndyConstants.Params.MENU_ID);
                    new FetchDetailsAsyncTask().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                Toast.makeText(DetailsMenuActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
            }

        }


}
