package com.frf.frfrestaurant;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.frf.frfrestaurant.Core.PreferenceHelper;

public class SplashActivity extends Activity {

    private PreferenceHelper preferenceHelper;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        Thread timer = new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(1000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    preferenceHelper = new PreferenceHelper(SplashActivity.this);
                    if(!preferenceHelper.getUserPass().isEmpty() && !preferenceHelper.getRID().isEmpty() && !preferenceHelper.getUsername().isEmpty()){//if the user previously logged in just send them to the home screen
                        preferenceHelper.putLoginSkipped("true");

                        Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        SplashActivity.this.finish();
                    }
                    else {
                        Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        SplashActivity.this.finish();
                    }
                }
            }
        };

        timer.start();
    }


}