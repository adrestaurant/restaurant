package com.frf.frfrestaurant;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.AndyConstants;

public class DetailsMenuHomeActivity extends AppCompatActivity {

    private Button menuDetailsButton, dishListingButton, addDishButton;
    private String menu_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_menu_home);

        //add the filter params to the hashmap that stores the request data
        Intent intent = getIntent();

       menu_id =  intent.getStringExtra(AndyConstants.Params.MENU_ID);
        Log.d("det men home menu_id1",menu_id);
        menuDetailsButton = findViewById(R.id.menuDetailsButton);
        dishListingButton = findViewById(R.id.dishListingButton);
        addDishButton = findViewById(R.id.addDishButton);


        menuDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsMenuHomeActivity.this, DetailsMenuActivity.class);
                intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
                Log.d("det men home menu_id2",menu_id);
                startActivity(intent);
            }
        });

        dishListingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsMenuHomeActivity.this, ListingDishActivity.class);
                intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
                Log.d("det men home menu_id3",menu_id);
                startActivity(intent);
            }
        });

        addDishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsMenuHomeActivity.this, AddDishActivity.class);
                intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
                Log.d("det men home menu_id4",menu_id);
                startActivity(intent);
            }
        });
    }

}
