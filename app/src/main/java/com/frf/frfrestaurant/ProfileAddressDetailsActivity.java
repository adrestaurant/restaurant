package com.frf.frfrestaurant;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;
import com.frf.frfrestaurant.ExpandableAdapters.ParishExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableAdapters.VillageExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableModels.DataItem;
import com.frf.frfrestaurant.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProfileAddressDetailsActivity extends AppCompatActivity {

    private ExpandableListView parishList, villageList;
    private ArrayList<DataItem> parishArCategory, villageArCategory;
    private ArrayList<SubCategoryItem> parishArSubCategory, villageArSubCategory;
    private ArrayList<HashMap<String, String>> parishParentItems, villageParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> parishChildItems, villageChildItems;
    private ParishExpandableListAdapter parishExpandableListAdapter;
    private VillageExpandableListAdapter villageExpandableListAdapter;
    private JSONArray parishes, villages;
    private String selectedParish, selectedVillage;
    private String selectedParishName, selectedVillageName;
    double longitude, latitude;
    private int childHeight;
    private JSONObject lists, newParish, newVillage;

    private EditText street;
    private Button updateButton;
    private TextView addParishLink, addVillageLink;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    int currentPID;
    int currentVID;
    String currentStreet;

    //for add modal
    private AlertDialog parishDialogBuilder, villageDialogBuilder;
    private EditText parishName, villageName;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_address_details);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);


        street = findViewById(R.id.street);

        updateButton = findViewById(R.id.updateButton);
        addParishLink = findViewById(R.id.addParishLink);
        addVillageLink = findViewById(R.id.addVillageLink);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    update();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        addParishLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createParishModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        addVillageLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createVillageModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        new FetchDetailsAsyncTask().execute();

    }

    private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ProfileAddressDetailsActivity.this)) {
                Toast.makeText(ProfileAddressDetailsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ProfileAddressDetailsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEADDRESSINDEX);
                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();

            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("detailsson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }


    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            lists = parseContent.getDataObj(result);
        } else {//if something goes wrong with the retrieval, put them back at the profile address home screen with an error toast
            Toast.makeText(ProfileAddressDetailsActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ProfileAddressDetailsActivity.this, ProfileAddressHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        Log.d("resukts", String.valueOf(lists));

        //current info
        try {
            JSONObject current = lists.getJSONArray("current").getJSONObject(0);
            currentPID = current.getInt(AndyConstants.Params.PID);
            currentVID = current.getInt(AndyConstants.Params.VID);
            currentStreet = current.getString(AndyConstants.Params.STREET);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //street
        street.setText(currentStreet);

//VILLAGES
        villageList = findViewById(R.id.villageList);

        DataItem villageItem = new DataItem();
        villageItem.setCategoryId("village");
        villageItem.setCategoryName("Village");

        villageArCategory = new ArrayList<>();
        villageArSubCategory = new ArrayList<>();
        villageParentItems = new ArrayList<>();
        villageChildItems = new ArrayList<>();

        try {
            villages = lists.getJSONArray(AndyConstants.Params.VILLAGES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < villages.length(); i++) {
            JSONObject village = null;
            try {
                village = villages.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("village");
                subCategoryItem.setSubId(String.valueOf(village.getInt(AndyConstants.Params.VID)));

                if(currentVID == village.getInt(AndyConstants.Params.VID)){//if the village is the current one
                    subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                }
                else{
                    subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                subCategoryItem.setSubCategoryName(village.getString(AndyConstants.Params.VILLAGE_NAME));
                villageArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        villageItem.setSubCategory(villageArSubCategory);
        villageArCategory.add(villageItem);

        for (DataItem data : villageArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            villageChildItems.add(childArrayList);
            villageParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = villageParentItems;
        ExpandableConstantManager.childItems = villageChildItems;

        Log.d("TAG", "village parent: " + villageParentItems);
        Log.d("TAG", "village children: " + villageChildItems);

        villageExpandableListAdapter = new VillageExpandableListAdapter(this, villageParentItems, villageChildItems, false);

        villageList.setAdapter(villageExpandableListAdapter);

        Log.d("TAG", "village parent: " + villageParentItems);
        Log.d("TAG", "village children: " + villageChildItems);

        villageList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("village children","count= "+villageList.getChildCount());
                for (int i = 0; i < villageList.getChildCount(); i++) {
                    childHeight = villageList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + villageList.getDividerHeight();
                }

                Log.d("count", String.valueOf(villageExpandableListAdapter.getChildrenCount(groupPosition)));
                villageList.getLayoutParams().height = (height) * (villageExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        villageList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                villageList.getLayoutParams().height = childHeight;
            }
        });

//PARISHES
        parishList = findViewById(R.id.parishList);
        Log.d("parishList", String.valueOf(parishList));

        DataItem parishItem = new DataItem();
        parishItem.setCategoryId("parish");
        parishItem.setCategoryName("Parish");

        parishArCategory = new ArrayList<>();
        parishArSubCategory = new ArrayList<>();
        parishParentItems = new ArrayList<>();
        parishChildItems = new ArrayList<>();

        try {
            parishes = lists.getJSONArray(AndyConstants.Params.PARISHES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < parishes.length(); i++) {
            JSONObject parish = null;
            try {
                parish = parishes.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("parish");
                subCategoryItem.setSubId(String.valueOf(parish.getInt(AndyConstants.Params.PID)));

                if(currentPID == parish.getInt(AndyConstants.Params.PID)){//if the parish is the current one
                    subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                }
                else{
                    subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                subCategoryItem.setSubCategoryName(parish.getString(AndyConstants.Params.PARISH_NAME));
                parishArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        parishItem.setSubCategory(parishArSubCategory);
        parishArCategory.add(parishItem);

        for (DataItem data : parishArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            parishChildItems.add(childArrayList);
            parishParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = parishParentItems;
        ExpandableConstantManager.childItems = parishChildItems;

        Log.d("TAG", "parish parent: " + parishParentItems);
        Log.d("TAG", "parish children: " + parishChildItems);

        parishExpandableListAdapter = new ParishExpandableListAdapter(this, parishParentItems, parishChildItems, false);

        parishList.setAdapter(parishExpandableListAdapter);

        Log.d("TAG", "parish parent: " + parishParentItems);
        Log.d("TAG", "parish children: " + parishChildItems);

        parishList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("parish children","count= "+parishList.getChildCount());
                for (int i = 0; i < parishList.getChildCount(); i++) {
                    childHeight = parishList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + parishList.getDividerHeight();
                }

                Log.d("count", String.valueOf(parishExpandableListAdapter.getChildrenCount(groupPosition)));
                parishList.getLayoutParams().height = (height) * (parishExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        parishList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                parishList.getLayoutParams().height = childHeight;
            }
        });
    }

    public void createParishModal()throws IOException, JSONException {
        Log.d("here","inside create parish modal");
        parishDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Parish");
        parishName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parishDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addParish();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                parishDialogBuilder.dismiss();
            }
        });

        parishDialogBuilder.setView(dialogView);
        parishDialogBuilder.show();

    }


    public void addParish()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(ProfileAddressDetailsActivity.this)) {
            Toast.makeText(ProfileAddressDetailsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileAddressDetailsActivity.this);
        final HashMap<String, String> parishMap = new HashMap<>();
        parishMap.put(AndyConstants.Params.PARISH_NAME,parishName.getText().toString());//get the parish name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PARISHESADD);
                    response = req.prepare(HttpRequest.Method.POST).withData(parishMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("parish res:", result);
                afterAddParish(result);
            }
        }.execute();
    }
    private void afterAddParish(String response) {
        Log.d("parishjson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        if (parseContent.isSuccess(response)) {
            Toast.makeText(ProfileAddressDetailsActivity.this, "Parish added!", Toast.LENGTH_SHORT).show();
            parishDialogBuilder.dismiss();

            //repopulate the parish list and select the new value
            lists = parseContent.getDataObj(response);


//        arSubCategory = new ArrayList<>();
//

//PARISHES
            parishList = findViewById(R.id.parishList);
            Log.d("parishList", String.valueOf(parishList));

            DataItem parishItem = new DataItem();
            parishItem.setCategoryId("parish");
            parishItem.setCategoryName("Parish");

            parishArCategory = new ArrayList<>();
            parishArSubCategory = new ArrayList<>();
            parishParentItems = new ArrayList<>();
            parishChildItems = new ArrayList<>();

            try {
                parishes = lists.getJSONArray(AndyConstants.Params.ALL);
                newParish = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < parishes.length(); i++) {
                JSONObject parish = null;
                try {
                    parish = parishes.getJSONObject(i);
                    SubCategoryItem subCategoryItem = new SubCategoryItem();
                    subCategoryItem.setCategoryId("parish");
                    subCategoryItem.setSubId(String.valueOf(parish.getInt(AndyConstants.Params.PID)));

                    if(newParish.getInt(AndyConstants.Params.PID) == parish.getInt(AndyConstants.Params.PID)){//if the parish is the new one
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    subCategoryItem.setSubCategoryName(parish.getString(AndyConstants.Params.PARISH_NAME));
                    parishArSubCategory.add(subCategoryItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            parishItem.setSubCategory(parishArSubCategory);
            parishArCategory.add(parishItem);

            for (DataItem data : parishArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> mapParent = new HashMap<String, String>();

                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                int countIsChecked = 0;
                for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                    HashMap<String, String> mapChild = new HashMap<String, String>();
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                    mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                    mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                    if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                        countIsChecked++;
                    }
                    childArrayList.add(mapChild);
                }

                if (countIsChecked == data.getSubCategory().size()) {

                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                } else {
                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                parishChildItems.add(childArrayList);
                parishParentItems.add(mapParent);

            }

            ExpandableConstantManager.parentItems = parishParentItems;
            ExpandableConstantManager.childItems = parishChildItems;

            Log.d("TAG", "parish parent: " + parishParentItems);
            Log.d("TAG", "parish children: " + parishChildItems);

            parishExpandableListAdapter = new ParishExpandableListAdapter(this, parishParentItems, parishChildItems, false);

            parishList.setAdapter(parishExpandableListAdapter);

            Log.d("TAG", "parish parent: " + parishParentItems);
            Log.d("TAG", "parish children: " + parishChildItems);

            parishList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int height = 0;
                    Log.d("parish children","count= "+parishList.getChildCount());
                    for (int i = 0; i < parishList.getChildCount(); i++) {
                        childHeight = parishList.getChildAt(i).getMeasuredHeight();
                        height = childHeight + parishList.getDividerHeight();
                    }

                    Log.d("count", String.valueOf(parishExpandableListAdapter.getChildrenCount(groupPosition)));
                    parishList.getLayoutParams().height = (height) * (parishExpandableListAdapter.getChildrenCount(groupPosition)+1);
                }
            });

            // Listview Group collapsed listener
            parishList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                    parishList.getLayoutParams().height = childHeight;
                }
            });

        }else {
            Toast.makeText(ProfileAddressDetailsActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }



    public void createVillageModal()throws IOException, JSONException {
        Log.d("here","inside create village modal");
        villageDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Village");
        villageName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                villageDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addVillage();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                villageDialogBuilder.dismiss();
            }
        });

        villageDialogBuilder.setView(dialogView);
        villageDialogBuilder.show();

    }


    public void addVillage()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(ProfileAddressDetailsActivity.this)) {
            Toast.makeText(ProfileAddressDetailsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileAddressDetailsActivity.this);
        final HashMap<String, String> villageMap = new HashMap<>();
        villageMap.put(AndyConstants.Params.VILLAGE_NAME,villageName.getText().toString());//get the village name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.VILLAGESADD);
                    response = req.prepare(HttpRequest.Method.POST).withData(villageMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("village res:", result);
                afterAddVillage(result);
            }
        }.execute();
    }
    private void afterAddVillage(String response) {
        Log.d("villagejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        if (parseContent.isSuccess(response)) {
            Toast.makeText(ProfileAddressDetailsActivity.this, "Village added!", Toast.LENGTH_SHORT).show();
            villageDialogBuilder.dismiss();

            //repopulate the village list and select the new value
            lists = parseContent.getDataObj(response);

//VILLAGES
            villageList = findViewById(R.id.villageList);
            Log.d("villageList", String.valueOf(villageList));

            DataItem villageItem = new DataItem();
            villageItem.setCategoryId("village");
            villageItem.setCategoryName("Village");

            villageArCategory = new ArrayList<>();
            villageArSubCategory = new ArrayList<>();
            villageParentItems = new ArrayList<>();
            villageChildItems = new ArrayList<>();

            try {
                villages = lists.getJSONArray(AndyConstants.Params.ALL);
                newVillage = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < villages.length(); i++) {
                JSONObject village = null;
                try {
                    village = villages.getJSONObject(i);
                    SubCategoryItem subCategoryItem = new SubCategoryItem();
                    subCategoryItem.setCategoryId("village");
                    subCategoryItem.setSubId(String.valueOf(village.getInt(AndyConstants.Params.VID)));

                    if(newVillage.getInt(AndyConstants.Params.VID) == village.getInt(AndyConstants.Params.VID)){//if the village is the new one
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    subCategoryItem.setSubCategoryName(village.getString(AndyConstants.Params.VILLAGE_NAME));
                    villageArSubCategory.add(subCategoryItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            villageItem.setSubCategory(villageArSubCategory);
            villageArCategory.add(villageItem);

            for (DataItem data : villageArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> mapParent = new HashMap<String, String>();

                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                int countIsChecked = 0;
                for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                    HashMap<String, String> mapChild = new HashMap<String, String>();
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                    mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                    mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                    if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                        countIsChecked++;
                    }
                    childArrayList.add(mapChild);
                }

                if (countIsChecked == data.getSubCategory().size()) {

                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                } else {
                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                villageChildItems.add(childArrayList);
                villageParentItems.add(mapParent);

            }

            ExpandableConstantManager.parentItems = villageParentItems;
            ExpandableConstantManager.childItems = villageChildItems;

            Log.d("TAG", "village parent: " + villageParentItems);
            Log.d("TAG", "village children: " + villageChildItems);

            villageExpandableListAdapter = new VillageExpandableListAdapter(this, villageParentItems, villageChildItems, false);

            villageList.setAdapter(villageExpandableListAdapter);

            Log.d("TAG", "village parent: " + villageParentItems);
            Log.d("TAG", "village children: " + villageChildItems);

            villageList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int height = 0;
                    Log.d("village children","count= "+villageList.getChildCount());
                    for (int i = 0; i < villageList.getChildCount(); i++) {
                        childHeight = villageList.getChildAt(i).getMeasuredHeight();
                        height = childHeight + villageList.getDividerHeight();
                    }

                    Log.d("count", String.valueOf(villageExpandableListAdapter.getChildrenCount(groupPosition)));
                    villageList.getLayoutParams().height = (height) * (villageExpandableListAdapter.getChildrenCount(groupPosition)+1);
                }
            });

            // Listview Group collapsed listener
            villageList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                    villageList.getLayoutParams().height = childHeight;
                }
            });

        }else {
            Toast.makeText(ProfileAddressDetailsActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }

    public void update()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(ProfileAddressDetailsActivity.this)) {
            Toast.makeText(ProfileAddressDetailsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileAddressDetailsActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.STREET,street.getText().toString());

        //selectedParish
        for (int i = 0; i < ParishExpandableListAdapter.parentItems.size(); i++ ) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < ParishExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                    selectedParishName = ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME);
                    Log.d("selectedParishName",selectedParishName);
                }

            }
            selectedParish = String.valueOf(children);
            Log.d("selectedParish",selectedParish);
        }
        map.put(AndyConstants.Params.PID,selectedParish);

        //selectedVillage
        for (int i = 0; i < VillageExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < VillageExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                    selectedVillageName = VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME);
                    Log.d("selectedVillageName",selectedVillageName);
                }
            }
            selectedVillage = String.valueOf(children);
            Log.d("selectedVillage",selectedVillage);
        }
        map.put(AndyConstants.Params.VID,selectedVillage);

        //geocode the street, village and parish to get the longitude and latitude
        String addr = street.getText() + ", " + selectedVillageName + ", " + selectedParishName + ", Barbados";

        Geocoder gc = new Geocoder(this);
        if(gc.isPresent()){
            List<Address> list = null;
            try {
                list = gc.getFromLocationName(addr, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = list.get(0);
            latitude = address.getLatitude();
            longitude = address.getLongitude();
        }

        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(longitude));
        map.put(AndyConstants.Params.LATITUDE, String.valueOf(latitude));

        Log.d("request: ", String.valueOf(map));

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEADDRESSUPDATE);
                    map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();

                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result);
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {
            Toast.makeText(ProfileAddressDetailsActivity.this, "Address Successfully Updated!", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(ProfileAddressDetailsActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }

}
