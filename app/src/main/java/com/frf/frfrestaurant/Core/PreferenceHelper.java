package com.frf.frfrestaurant.Core;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    public static final String MyPREFERENCES = "MyPrefs" ;
    private final String USERNAME = "username";
    private final String PASSWORD = "password";
    private final String USERPASS = "userpass";
    private final String RID = "rid";
    private final String LOGINSKIPPED = "login_skipped";
    private SharedPreferences sharedpreferences;
    private Context context;

    public PreferenceHelper(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putUsername(String uname) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(USERNAME, uname);
         edit.commit();
    }
    public String getUsername() {
        return sharedpreferences.getString(USERNAME, "").trim();
    }

    public void putPassword(String pword) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(PASSWORD, pword);
         edit.commit();
    }
    public String getPassword() {
        return sharedpreferences.getString(PASSWORD, "").trim();
    }

    public void putUserPass(String up) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(USERPASS, up);
         edit.commit();
    }
    public String getUserPass() {
        return sharedpreferences.getString(USERPASS, "").trim();
    }

    public void putRID(String rid) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(RID, rid);
        edit.commit();
    }
    public String getRID() {
        return sharedpreferences.getString(RID, "").trim();
    }

    public String getLoginSkipped() {
        return sharedpreferences.getString(LOGINSKIPPED, "").trim();
    }

    public void putLoginSkipped(String val) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(LOGINSKIPPED, val);
        edit.commit();
    }


    public void clearPrefs() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

}
