package com.frf.frfrestaurant.Core;

public class AndyConstants {

    // web service url constants
    public class ServiceType {
//        Base URL
//        public static final String BASE_URL = "http://10.22.6.9/ad-api-full-test/restaurant/";
//        public static final String BASE_URL = "http://172.30.20.81/ad-api-full-test/restaurant/";

        public static final String BASE_URL = "https://findrestfood.000webhostapp.com/restaurant/";

//        base for image folder
//        public static final String IMAGES = "http://10.22.6.9/ad-api-full-test/";
//        public static final String IMAGES = "http://172.30.20.81/ad-api-full-test/";

        public static final String IMAGES = "https://findrestfood.000webhostapp.com/";

//        Login link
        public static final String LOGIN = BASE_URL + "login";//log the user in

//        Sign Up links
        public static final String SIGNUPINDEX =  BASE_URL + "signup/index";//get all the parishes and villages to populate the expandables
        public static final String SIGNUPACTION =  BASE_URL + "signup/signup";//sign up the restaurant

        //      Ratings links
        public static final String RATINGSINDEX = BASE_URL + "ratings/index";//get all the ratings based on rid

        //      Menus links
        public static final String MENUSINDEX = BASE_URL + "menus/index";//gets all the menus for the listing page
        public static final String MENUSADD = BASE_URL + "menus/add";//used to create a menu
        public static final String MENUSDETAILS= BASE_URL + "menus/details";//get all the details for the menu details screen
        public static final String MENUSUPDATE = BASE_URL + "menus/update";//used to update a menu's details

        //      Dishes links
        public static final String DISHESINDEX = BASE_URL + "dishes/index";//for the listing activity
        public static final String DISHESADDINDEX= BASE_URL + "dishes/addindex";//used to get all the values for the expandables on the add screen
        public static final String DISHESADD = BASE_URL + "dishes/add";//used to create a dish
        public static final String DISHESUPDATE = BASE_URL + "dishes/update";//used to update a dish's details
        public static final String DISHESDETAILS = BASE_URL + "dishes/details";//get all the details for the dish details screen along with all the values for the expandables
        public static final String DISHESARCHIVE = BASE_URL + "dishes/archive";//used to archive a dish (accessed via the details screen)
        public static final String DISHESRESTORE = BASE_URL + "dishes/restore";//used to restore an archived dish (accessed via the details screen)

        //      Profile links
        public static final String PROFILEUSERNAMEINDEX = BASE_URL + "profile/usernameindex";//gets the restaurant's username
        public static final String PROFILEUSERNAMEUPDATE = BASE_URL + "profile/usernameupdate";//updates the restaurant's username
        public static final String PROFILECONTACTINDEX = BASE_URL + "profile/contactindex";//gets the restaurant's contact info
        public static final String PROFILECONTACTUPDATE = BASE_URL + "profile/contactupdate";//updates the restaurant's contact info
        public static final String PROFILEDETAILSINDEX = BASE_URL + "profile/detailsindex";//gets the restaurant's details
        public static final String PROFILEDETAILSUPDATE = BASE_URL + "profile/detailsupdate";//updates the restaurant's details
        public static final String PROFILEADDRESSINDEX = BASE_URL + "profile/addressindex";//gets the restaurant's address info
        public static final String PROFILEADDRESSUPDATE = BASE_URL + "profile/addressupdate";//updates the restaurant's address info
        public static final String PROFILECOORDINATESINDEX = BASE_URL + "profile/coordinatesindex";//gets the address info including parish and village names
        public static final String PROFILECOORDINATESUPDATE = BASE_URL + "profile/coordinatesupdate";//updates the restaurant's coordinates (use in address choose exact location activity)


        //Cuisine Links
        public static final String CUISINEADD = BASE_URL + "cuisine/add";//used to add a new cuisine
        public static final String CUISINEINDEX = BASE_URL + "cuisine/index";//used to fetch all cuisines from the db

        //Diet Types Links
        public static final String DIETTYPESADD = BASE_URL + "diettypes/add";//used to add a new diet type
        public static final String DIETTYPESINDEX = BASE_URL + "diettypes/index";//used to fetch all diet types from the db

        //Food Categories Links
        public static final String FOODCATEGORIESADD = BASE_URL + "foodcategories/add";//used to add a new food category
        public static final String FOODCATEGORIESINDEX = BASE_URL + "foodcategories/index";//used to fetch all food categories from the db

        //Meals Links
        public static final String MEALSINDEX = BASE_URL + "meals/index";//used to fetch all meals from the db
        public static final String MEALSADD = BASE_URL + "meals/add";//used to add a new meal

        //Parishes Links
        public static final String PARISHESADD = BASE_URL + "parishes/add";//used to add a new parish

        //Villages Links
        public static final String VILLAGESADD = BASE_URL + "villages/add";//used to add a new village


    }
    // webservice key constants
    public class Params {

        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String PASSWORDCONFIRM = "passwordConfirm";
        public static final String EMAIL = "email";
        public static final String F_NAME = "f_name";
        public static final String L_NAME = "l_name";
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
        public static  final String UID = "uid";
        public static  final String DT_ID = "dt_id";
        public static  final String PID = "pid";
        public static  final String VID = "vid";
        public static final String DT_NAME = "name";
        public static final String PARISH_NAME = "name";
        public static final String VILLAGE_NAME = "name";
        public static final String CUISINE_NAME = "name";

        public static final String RESTAURANT_NAME = "restaurant_name";
        public static final String CUISINE = "cuisine";
        public static final String OPEN_TIME = "open_time";
        public static final String CLOSE_TIME = "close_time";
        public static final String RID = "rid";
        public static final String STREET = "street";
        public static final String VILLAGE = "village";
        public static final String PARISH = "parish";
        public static final String DISTANCE = "distance";
        public static final String CALLING_CLASS = "calling_class";
        public static final String DESCRIPTION = "description";
        public static final String CONTACT_PERSON_F_NAME = "contact_person_f_name";
        public static final String CONTACT_PERSON_L_NAME = "contact_person_l_name";
        public static final String TELEPHONE_NUMBER = "telephone_number";
        public static final String NEARME = "nearme";
        public static final String DIET_TYPE = "diet_type";
        public static final String USER_DIET_PREFERENCES = "user_diet_preferences";
        public static final String MIN_PRICE = "min_price";
        public static final String MAX_PRICE = "max_price";
        public static final String PARISHES = "parishes";
        public static final String VILLAGES = "villages";
        public static final String CU_ID = "cu_id";
        public static final String MENU_ID = "menu_id";
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
        public static final String IMAGE_URL = "image_url";
        public static final String MEAL_NAME = "meal_name";
        public static final String DB_MEAL_NAME = "name";//use if getting value from MEALINDEX
        public static final String RATINGS = "ratings";
        public static final String RATING = "rating";
        public static final String COMMENT = "comment";
        public static final String CREATED_AT = "created_at";
        public static final String UPDATED_AT = "updated_at";
        public static final String ALL = "all";
        public static final String SELECTED = "selected";
        public static final String MEAL = "meal";
        public static final String MEAL_ID = "meal_id";
        public static final String DISH_NAME = "name";
        public static final String FOOD_CATEGORY = "food_category";
        public static final String FC_ID = "fc_id";
        public static final String PRICE = "price";
        public static final String D_ID = "d_id";
        public static final String DISH_IMAGE = "dish_image";
        public static final String DB_DISH_NAME = "name" ;
        public static final String DB_MENU_NAME = "menu_name" ;
        public static final String DB_FC_NAME = "name" ;
        public static final String DB_DT_NAME = "name";
        public static final String DB_CU_NAME = "name";
        public static final String MENUS = "menus";
        public static final String FOOD_CATEGORIES = "food_categories";
        public static final String DIET_TYPES = "diet_types";
        public static final String CUISINES = "cuisines";
        public static final String DELETED = "deleted";
    }

}
