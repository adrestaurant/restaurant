package com.frf.frfrestaurant.Core;

public interface AsyncTaskCompleteListener {
    void onTaskCompleted(String response, int serviceCode);
}