package com.frf.frfrestaurant;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.PreferenceHelper;

public class HomeActivity extends AppCompatActivity {

    TextView greeting;
    Button profileButton, menusButton, ratingsButton, logoutButton;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        preferenceHelper = new PreferenceHelper(this);

        if(preferenceHelper.getLoginSkipped().equals("true")){//if they came here straight from the splash screen show a welcome toast
            Toast.makeText(HomeActivity.this, "Welcome back "+preferenceHelper.getUsername()+"!", Toast.LENGTH_SHORT).show();
            preferenceHelper.putLoginSkipped("false");
        }

        greeting = findViewById(R.id.greeting);
        greeting.setText("Hey " + preferenceHelper.getUsername() + "!");

        profileButton = findViewById(R.id.profileButton);
        menusButton = findViewById(R.id.menusButton);
        ratingsButton = findViewById(R.id.ratingsButton);
        logoutButton = findViewById(R.id.logoutButton);


        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ProfileHomeActivity.class);
                startActivity(intent);
            }
        });

        menusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ListingMenuActivity.class);
                startActivity(intent);
            }
        });

        ratingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ListingRatingsActivity.class);
                startActivity(intent);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferenceHelper.clearPrefs();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                HomeActivity.this.finish();
            }
        });
    }

}
