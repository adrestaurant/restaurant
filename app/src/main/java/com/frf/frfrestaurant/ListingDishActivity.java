package com.frf.frfrestaurant;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfrestaurant.CardAdapters.DishCardAdapter;
import com.frf.frfrestaurant.CardModels.DishModel;
import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListingDishActivity extends AppCompatActivity {

    private TextView meal_name, noActiveDishesTV, noArchivedDishesTV;
    private ImageView menuImage;
    private Button addDishButton;
    private RecyclerView activeRecyclerView, archivedRecyclerView;

    private ArrayList<DishModel> activeDishModelArrayList, archivedDishModelArrayList;
    private DishCardAdapter activeAdapter, archivedAdapter;
    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private String menu_id;

    private HashMap<String, String> map;
    private JSONObject menu_info;
    private JSONArray active, archived;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_dishes);

        Intent intent = getIntent();
        menu_id =  intent.getStringExtra(AndyConstants.Params.MENU_ID);
        Log.d("det men menu_id",menu_id);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        menuImage = findViewById(R.id.menuImage);
        meal_name = findViewById(R.id.meal_name);

        activeRecyclerView = findViewById(R.id.activeRecyclerView);
        archivedRecyclerView = findViewById(R.id.archivedRecyclerView);

        addDishButton = findViewById(R.id.addDishButton);

        map = new HashMap<>();


        new FetchDishesAsyncTask().execute();

        activeRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), activeRecyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Toast.makeText(ListingDishActivity.this, activeDishModelArrayList.get(position).getDishName(), Toast.LENGTH_SHORT).show();
                String d_id = activeDishModelArrayList.get(position).getDID();
                Intent intent = new Intent(ListingDishActivity.this, DetailsDishActivity.class);
                intent.putExtra(AndyConstants.Params.D_ID,d_id);
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        archivedRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), activeRecyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Toast.makeText(ListingDishActivity.this, archivedDishModelArrayList.get(position).getDishName(), Toast.LENGTH_SHORT).show();
                String d_id = archivedDishModelArrayList.get(position).getDID();
                Intent intent = new Intent(ListingDishActivity.this, DetailsDishActivity.class);
                intent.putExtra(AndyConstants.Params.D_ID,d_id);
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        addDishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingDishActivity.this, AddDishActivity.class);
                intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
                startActivity(intent);
            }
        });

    }


    /**
     * Fetches the list of dishes from the server (based on rid)
     */
    private class FetchDishesAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ListingDishActivity.this)) {
                Toast.makeText(ListingDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ListingDishActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.DISHESINDEX);

                map.put(AndyConstants.Params.MENU_ID, menu_id);

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("dishesjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            try {
                menu_info = parseContent.getDataObj(result).getJSONObject("menu_info");//get the menu info from the results
                meal_name.setText(menu_info.getString("meal_name"));//set the meal name
                //get the image
                Picasso.get().load(Uri.parse(AndyConstants.ServiceType.IMAGES + menu_info.getString("menu_image")))
                        .resize(600, 300)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder_error)
                        .into(menuImage);

                if(parseContent.getDataObj(result).getJSONObject("dishes").getJSONArray("active").length() == 0){//if there are no active results show the text view
                    showNoActiveDishes();
                }
                else {//if there are results, show them
                    populateActiveList(result);
                }

                if(parseContent.getDataObj(result).getJSONObject("dishes").getJSONArray("archived").length() == 0){//if there are no active results show the text view
                    showNoArchivedDishes();
                }
                else {//if there are results, show them
                    populateArchivedList(result);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void showNoActiveDishes(){
        TextView noActiveDishesTV = findViewById(R.id.noActiveDishesTV);
        noActiveDishesTV.setVisibility(View.VISIBLE);
    }

    private void showNoArchivedDishes(){
        TextView noArchivedDishesTV = findViewById(R.id.noArchivedDishesTV);
        noArchivedDishesTV.setVisibility(View.VISIBLE);
    }

    private ArrayList<DishModel> populateActiveList(String result){

        activeDishModelArrayList = new ArrayList<DishModel>();

        Log.d("received rest info", result);
        if (parseContent.isSuccess(result)) {
            try {
                active = parseContent.getDataObj(result).getJSONObject("dishes").getJSONArray("active");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for(int i = 0; i < active.length(); i++) {
                JSONObject dish = null;
                try {
                    dish = active.getJSONObject(i);
                    DishModel dishModel = new DishModel();

                    dishModel.setDishName(dish.getString(AndyConstants.Params.DISH_NAME));
                    dishModel.setDietType(dish.getString(AndyConstants.Params.DIET_TYPE));
                    dishModel.setCuisine(dish.getString(AndyConstants.Params.CUISINE));
                    dishModel.setFoodCategory(dish.getString(AndyConstants.Params.FOOD_CATEGORY));
                    dishModel.setPrice(dish.getString(AndyConstants.Params.PRICE));
                    dishModel.setDID(dish.getString(AndyConstants.Params.D_ID));
                    dishModel.setImageUrl(dish.getString(AndyConstants.Params.DISH_IMAGE));

                    activeDishModelArrayList.add(dishModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d("number of active dishes", activeDishModelArrayList.size() + "");

            activeAdapter = new DishCardAdapter(this,activeDishModelArrayList);
            activeRecyclerView.setAdapter(activeAdapter);
            activeRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        }else {//if something goes wrong with the retrieval, put them back at the menu details home screen with an error toast
            Toast.makeText(ListingDishActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ListingDishActivity.this, DetailsMenuHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
            startActivity(intent);
            this.finish();
        }


        return activeDishModelArrayList;
    }

    private ArrayList<DishModel> populateArchivedList(String result){

        archivedDishModelArrayList = new ArrayList<DishModel>();

        Log.d("received rest info", result);
        if (parseContent.isSuccess(result)) {
            try {
                archived = parseContent.getDataObj(result).getJSONObject("dishes").getJSONArray("archived");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for(int i = 0; i < archived.length(); i++) {
                JSONObject dish = null;
                try {
                    dish = archived.getJSONObject(i);
                    DishModel dishModel = new DishModel();

                    dishModel.setDishName(dish.getString(AndyConstants.Params.DISH_NAME));
                    dishModel.setDietType(dish.getString(AndyConstants.Params.DIET_TYPE));
                    dishModel.setCuisine(dish.getString(AndyConstants.Params.CUISINE));
                    dishModel.setFoodCategory(dish.getString(AndyConstants.Params.FOOD_CATEGORY));
                    dishModel.setPrice(dish.getString(AndyConstants.Params.PRICE));
                    dishModel.setDID(dish.getString(AndyConstants.Params.D_ID));
                    dishModel.setImageUrl(dish.getString(AndyConstants.Params.DISH_IMAGE));

                    archivedDishModelArrayList.add(dishModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d("# of archived dishes", archivedDishModelArrayList.size() + "");

            archivedAdapter = new DishCardAdapter(this,archivedDishModelArrayList);
            archivedRecyclerView.setAdapter(archivedAdapter);
            archivedRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        }else {//if something goes wrong with the retrieval, put them back at the menu details home screen with an error toast
            Toast.makeText(ListingDishActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ListingDishActivity.this, DetailsMenuHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
            startActivity(intent);
            this.finish();
        }


        return archivedDishModelArrayList;
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
