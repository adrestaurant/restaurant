package com.frf.frfrestaurant;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.AsyncTaskCompleteListener;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.MultiPartRequester;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;
import com.frf.frfrestaurant.ExpandableAdapters.CuisineExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableAdapters.DietPrefsExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableAdapters.FoodCategoryExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableAdapters.MenuExpandableListAdapter;
import com.frf.frfrestaurant.ExpandableModels.DataItem;
import com.frf.frfrestaurant.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class AddDishActivity extends AppCompatActivity implements AsyncTaskCompleteListener {

    private ExpandableListView menuList, foodCategoryList, dietTypeList, cuisineList;
    private ArrayList<DataItem> menuArCategory, foodCategoryArCategory, dietTypeArCategory, cuisineArCategory;
    private ArrayList<SubCategoryItem> menuArSubCategory, foodCategoryArSubCategory, dietTypeArSubCategory, cuisineArSubCategory ;
    private ArrayList<HashMap<String, String>> menuParentItems, foodCategoryParentItems, dietTypeParentItems, cuisineParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> menuChildItems, foodCategoryChildItems, dietTypeChildItems, cuisineChildItems;
    private MenuExpandableListAdapter menuExpandableListAdapter;
    private FoodCategoryExpandableListAdapter foodCategoryExpandableListAdapter;
    private DietPrefsExpandableListAdapter dietTypeExpandableListAdapter;
    private CuisineExpandableListAdapter cuisineExpandableListAdapter;
    private JSONArray menus, foodCategories, dietTypes, cuisines;
    private String selectedMenu, selectedFoodCategory, selectedDietType, selectedCuisine;
    private int childHeight;
    private JSONObject lists;
    private JSONObject newFoodCategory, newDietType, newCuisine;

    private EditText name, description, price;

    private String menu_id;

    private ImageView image;
    private Button chooseButton, addButton;
    private TextView addFoodCategoryLink,addDietTypeLink,addCuisineLink;

    private final int GALLERY = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private Uri path;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    //for add modal
    private AlertDialog  foodCategoryDialogBuilder, dietTypeDialogBuilder, cuisineDialogBuilder;
    private EditText  foodCategoryName, dietTypeName, cuisineName;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dish);

        //Requesting storage permission
        requestStoragePermission();

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        image = findViewById(R.id.image);

        Intent intent = getIntent();
        menu_id =  intent.getStringExtra(AndyConstants.Params.MENU_ID);
        Log.d("add dish menu_id",menu_id);

        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        price = findViewById(R.id.price);


        addFoodCategoryLink = findViewById(R.id.addFoodCategoryLink);
        addDietTypeLink = findViewById(R.id.addDietTypeLink);
        addCuisineLink = findViewById(R.id.addCuisineLink);
        chooseButton = findViewById(R.id.chooseButton);
        addButton = findViewById(R.id.addDishButton);



        addFoodCategoryLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createFoodCategoryModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        addDietTypeLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createDietTypeModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        addCuisineLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createCuisineModal();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDish();
            }
        });

        new FetchListsAsyncTask().execute();

    }

    private class FetchListsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(AddDishActivity.this)) {
                Toast.makeText(AddDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(AddDishActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.DISHESADDINDEX);
                final HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("expandablesjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }

    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            lists = parseContent.getDataObj(result);
            try {
                menus = lists.getJSONArray(AndyConstants.Params.MENUS);
                foodCategories = lists.getJSONArray(AndyConstants.Params.FOOD_CATEGORIES);
                dietTypes = lists.getJSONArray(AndyConstants.Params.DIET_TYPES);
                cuisines = lists.getJSONArray(AndyConstants.Params.CUISINE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {//if something goes wrong with the retrieval, put them back at the listing screen with an error toast
            Toast.makeText(AddDishActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(AddDishActivity.this, ListingDishActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
        Log.d("vals", String.valueOf(lists));

        //MENUS
        menuList = findViewById(R.id.menuList);

        DataItem menuItem = new DataItem();
        menuItem.setCategoryId("menu");
        menuItem.setCategoryName("Menu");

        menuArCategory = new ArrayList<>();
        menuArSubCategory = new ArrayList<>();
        menuParentItems = new ArrayList<>();
        menuChildItems = new ArrayList<>();

        for (int i = 0; i < menus.length(); i++) {
            JSONObject menu = null;
            try {
                menu = menus.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("menu");
                subCategoryItem.setSubId(String.valueOf(menu.getInt(AndyConstants.Params.MENU_ID)));

                if(Integer.parseInt(menu_id) == menu.getInt(AndyConstants.Params.MENU_ID)){//select the current menu by default
                    subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                }
                else{
                    subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                subCategoryItem.setSubCategoryName(menu.getString(AndyConstants.Params.MEAL_NAME));
                menuArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        menuItem.setSubCategory(menuArSubCategory);
        menuArCategory.add(menuItem);

        for (DataItem data : menuArCategory) {
            //                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            menuChildItems.add(childArrayList);
            menuParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = menuParentItems;
        ExpandableConstantManager.childItems = menuChildItems;

        Log.d("TAG", "menu parent: " + menuParentItems);
        Log.d("TAG", "menu children: " + menuChildItems);

        menuExpandableListAdapter = new MenuExpandableListAdapter(this, menuParentItems, menuChildItems, false);

        menuList.setAdapter(menuExpandableListAdapter);

        Log.d("TAG", "menu parent: " + menuParentItems);
        Log.d("TAG", "menu children: " + menuChildItems);

        menuList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                for (int i = 0; i < menuList.getChildCount(); i++) {
                    childHeight = menuList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + menuList.getDividerHeight();
                }
                menuList.getLayoutParams().height = height * (menuExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        menuList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                menuList.getLayoutParams().height = childHeight;
            }
        });


        //FOOD CATEGORIES
        foodCategoryList = findViewById(R.id.foodCategoryList);

        DataItem foodCategoryItem = new DataItem();
        foodCategoryItem.setCategoryId("food_category");
        foodCategoryItem.setCategoryName("Food Category");

        foodCategoryArCategory = new ArrayList<>();
        foodCategoryArSubCategory = new ArrayList<>();
        foodCategoryParentItems = new ArrayList<>();
        foodCategoryChildItems = new ArrayList<>();

        for (int i = 0; i < foodCategories.length(); i++) {
            JSONObject foodCategory = null;
            try {
                foodCategory = foodCategories.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("food_category");
                subCategoryItem.setSubId(String.valueOf(foodCategory.getInt(AndyConstants.Params.FC_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(foodCategory.getString(AndyConstants.Params.DB_FC_NAME));
                foodCategoryArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        foodCategoryItem.setSubCategory(foodCategoryArSubCategory);
        foodCategoryArCategory.add(foodCategoryItem);

        for (DataItem data : foodCategoryArCategory) {
            //                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            foodCategoryChildItems.add(childArrayList);
            foodCategoryParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = foodCategoryParentItems;
        ExpandableConstantManager.childItems = foodCategoryChildItems;

        Log.d("TAG", "foodCategory parent: " + foodCategoryParentItems);
        Log.d("TAG", "foodCategory children: " + foodCategoryChildItems);

        foodCategoryExpandableListAdapter = new FoodCategoryExpandableListAdapter(this, foodCategoryParentItems, foodCategoryChildItems, false);

        foodCategoryList.setAdapter(foodCategoryExpandableListAdapter);

        Log.d("TAG", "foodCategory parent: " + foodCategoryParentItems);
        Log.d("TAG", "foodCategory children: " + foodCategoryChildItems);

        foodCategoryList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                for (int i = 0; i < foodCategoryList.getChildCount(); i++) {
                    childHeight = foodCategoryList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + foodCategoryList.getDividerHeight();
                }
                foodCategoryList.getLayoutParams().height = height * (foodCategoryExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        foodCategoryList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                foodCategoryList.getLayoutParams().height = childHeight;
            }
        });


        //DIET TYPES
        dietTypeList = findViewById(R.id.dietTypeList);

        DataItem dietTypeItem = new DataItem();
        dietTypeItem.setCategoryId("diet_types");
        dietTypeItem.setCategoryName("Diet Type");

        dietTypeArCategory = new ArrayList<>();
        dietTypeArSubCategory = new ArrayList<>();
        dietTypeParentItems = new ArrayList<>();
        dietTypeChildItems = new ArrayList<>();

        for (int i = 0; i < dietTypes.length(); i++) {
            JSONObject dietType = null;
            try {
                dietType = dietTypes.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("diet_types");
                subCategoryItem.setSubId(String.valueOf(dietType.getInt(AndyConstants.Params.DT_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(dietType.getString(AndyConstants.Params.DB_DT_NAME));
                dietTypeArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        dietTypeItem.setSubCategory(dietTypeArSubCategory);
        dietTypeArCategory.add(dietTypeItem);

        for (DataItem data : dietTypeArCategory) {
            //                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            dietTypeChildItems.add(childArrayList);
            dietTypeParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = dietTypeParentItems;
        ExpandableConstantManager.childItems = dietTypeChildItems;

        Log.d("TAG", "dietType parent: " + dietTypeParentItems);
        Log.d("TAG", "dietType children: " + dietTypeChildItems);

        dietTypeExpandableListAdapter = new DietPrefsExpandableListAdapter(this, dietTypeParentItems, dietTypeChildItems, false);

        dietTypeList.setAdapter(dietTypeExpandableListAdapter);

        Log.d("TAG", "dietType parent: " + dietTypeParentItems);
        Log.d("TAG", "dietType children: " + dietTypeChildItems);

        dietTypeList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("dietType children","count= "+dietTypeList.getChildCount());
                for (int i = 0; i < dietTypeList.getChildCount(); i++) {
                    childHeight = dietTypeList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + dietTypeList.getDividerHeight();
                }

                Log.d("count", String.valueOf(dietTypeExpandableListAdapter.getChildrenCount(groupPosition)));
                dietTypeList.getLayoutParams().height = (height) * (dietTypeExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        dietTypeList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                dietTypeList.getLayoutParams().height = childHeight;
            }
        });

        //CUISINES
        cuisineList = findViewById(R.id.cuisineList);

        DataItem cuisineItem = new DataItem();
        cuisineItem.setCategoryId("cuisine");
        cuisineItem.setCategoryName("Cuisine");

        cuisineArCategory = new ArrayList<>();
        cuisineArSubCategory = new ArrayList<>();
        cuisineParentItems = new ArrayList<>();
        cuisineChildItems = new ArrayList<>();

        for (int i = 0; i < cuisines.length(); i++) {
            JSONObject cuisine = null;
            try {
                cuisine = cuisines.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("cuisine");
                subCategoryItem.setSubId(String.valueOf(cuisine.getInt(AndyConstants.Params.CU_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(cuisine.getString(AndyConstants.Params.DB_CU_NAME));
                cuisineArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        cuisineItem.setSubCategory(cuisineArSubCategory);
        cuisineArCategory.add(cuisineItem);

        for (DataItem data : cuisineArCategory) {
            //                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
            cuisineChildItems.add(childArrayList);
            cuisineParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = cuisineParentItems;
        ExpandableConstantManager.childItems = cuisineChildItems;

        Log.d("TAG", "cuisine parent: " + cuisineParentItems);
        Log.d("TAG", "cuisine children: " + cuisineChildItems);

        cuisineExpandableListAdapter = new CuisineExpandableListAdapter(this, cuisineParentItems, cuisineChildItems, false);

        cuisineList.setAdapter(cuisineExpandableListAdapter);

        Log.d("TAG", "cuisine parent: " + cuisineParentItems);
        Log.d("TAG", "cuisine children: " + cuisineChildItems);

        cuisineList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                for (int i = 0; i < cuisineList.getChildCount(); i++) {
                    childHeight = cuisineList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + cuisineList.getDividerHeight();
                }
                cuisineList.getLayoutParams().height = (height) * (cuisineExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        cuisineList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                cuisineList.getLayoutParams().height = childHeight;
            }
        });

    }



    public void createFoodCategoryModal()throws IOException, JSONException {
        Log.d("here","inside create food category modal");
        foodCategoryDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Food Category");
        foodCategoryName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodCategoryDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addFoodCategory();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                foodCategoryDialogBuilder.dismiss();
            }
        });

        foodCategoryDialogBuilder.setView(dialogView);
        foodCategoryDialogBuilder.show();

    }


    public void addFoodCategory()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(AddDishActivity.this)) {
            Toast.makeText(AddDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(AddDishActivity.this);
        final HashMap<String, String> foodCategoryMap = new HashMap<>();
        foodCategoryMap.put(AndyConstants.Params.DB_FC_NAME,foodCategoryName.getText().toString());//get the food category name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.FOODCATEGORIESADD);

                    foodCategoryMap.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(foodCategoryMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("food category res:", result);
                afterAddFoodCategory(result);
            }
        }.execute();
    }

    private void afterAddFoodCategory(String response) {
        Log.d("foodCategoryjson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        if (parseContent.isSuccess(response)) {
            Toast.makeText(AddDishActivity.this, "Food Category added!", Toast.LENGTH_SHORT).show();
            foodCategoryDialogBuilder.dismiss();

            //repopulate the food category list and select the new value
            lists = parseContent.getDataObj(response);

            //FOOD CATEGORIES
            foodCategoryList = findViewById(R.id.foodCategoryList);
            Log.d("foodCategoryList", String.valueOf(foodCategoryList));

            DataItem foodCategoryItem = new DataItem();
            foodCategoryItem.setCategoryId("food_category");
            foodCategoryItem.setCategoryName("Food Category");

            foodCategoryArCategory = new ArrayList<>();
            foodCategoryArSubCategory = new ArrayList<>();
            foodCategoryParentItems = new ArrayList<>();
            foodCategoryChildItems = new ArrayList<>();

            try {
                foodCategories = lists.getJSONArray(AndyConstants.Params.ALL);
                newFoodCategory = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < foodCategories.length(); i++) {
                JSONObject foodCategory = null;
                try {
                    foodCategory = foodCategories.getJSONObject(i);
                    SubCategoryItem subCategoryItem = new SubCategoryItem();
                    subCategoryItem.setCategoryId("food_category");
                    subCategoryItem.setSubId(String.valueOf(foodCategory.getInt(AndyConstants.Params.FC_ID)));

                    if(newFoodCategory.getInt(AndyConstants.Params.FC_ID) == foodCategory.getInt(AndyConstants.Params.FC_ID)){//if the food category is the new one
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    subCategoryItem.setSubCategoryName(foodCategory.getString(AndyConstants.Params.DB_FC_NAME));
                    foodCategoryArSubCategory.add(subCategoryItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            foodCategoryItem.setSubCategory(foodCategoryArSubCategory);
            foodCategoryArCategory.add(foodCategoryItem);

            for (DataItem data : foodCategoryArCategory) {
                //                        Log.i("Item", String.valueOf(arCategory));
                ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> mapParent = new HashMap<String, String>();

                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                int countIsChecked = 0;
                for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                    HashMap<String, String> mapChild = new HashMap<String, String>();
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                    mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                    mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                    if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                        countIsChecked++;
                    }
                    childArrayList.add(mapChild);
                }

                if (countIsChecked == data.getSubCategory().size()) {

                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                } else {
                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                foodCategoryChildItems.add(childArrayList);
                foodCategoryParentItems.add(mapParent);

            }

            ExpandableConstantManager.parentItems = foodCategoryParentItems;
            ExpandableConstantManager.childItems = foodCategoryChildItems;

            Log.d("TAG", "foodCategory parent: " + foodCategoryParentItems);
            Log.d("TAG", "foodCategory children: " + foodCategoryChildItems);

            foodCategoryExpandableListAdapter = new FoodCategoryExpandableListAdapter(this, foodCategoryParentItems, foodCategoryChildItems, false);

            foodCategoryList.setAdapter(foodCategoryExpandableListAdapter);

            Log.d("TAG", "foodCategory parent: " + foodCategoryParentItems);
            Log.d("TAG", "foodCategory children: " + foodCategoryChildItems);

            foodCategoryList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int height = 0;
                    for (int i = 0; i < foodCategoryList.getChildCount(); i++) {
                        childHeight = foodCategoryList.getChildAt(i).getMeasuredHeight();
                        height = childHeight + foodCategoryList.getDividerHeight();
                    }
                    foodCategoryList.getLayoutParams().height = height * (foodCategoryExpandableListAdapter.getChildrenCount(groupPosition)+1);
                }
            });

            // Listview Group collapsed listener
            foodCategoryList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                    foodCategoryList.getLayoutParams().height = childHeight;
                }
            });

        }else {
            Toast.makeText(AddDishActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }



    public void createDietTypeModal()throws IOException, JSONException {
        Log.d("here","inside create dietType modal");
        dietTypeDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Diet Type");
        dietTypeName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dietTypeDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addDietType();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dietTypeDialogBuilder.dismiss();
            }
        });

        dietTypeDialogBuilder.setView(dialogView);
        dietTypeDialogBuilder.show();

    }


    public void addDietType()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(AddDishActivity.this)) {
            Toast.makeText(AddDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(AddDishActivity.this);
        final HashMap<String, String> dietTypeMap = new HashMap<>();
        dietTypeMap.put(AndyConstants.Params.DB_DT_NAME,dietTypeName.getText().toString());//get the diet type name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.DIETTYPESADD);

                    dietTypeMap.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(dietTypeMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("dietType res:", result);
                afterAddDietType(result);
            }
        }.execute();
    }

    private void afterAddDietType(String response) {
        Log.d("dietTypejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        if (parseContent.isSuccess(response)) {
            Toast.makeText(AddDishActivity.this, "Diet Type added!", Toast.LENGTH_SHORT).show();
            dietTypeDialogBuilder.dismiss();

            //repopulate the dietType list and select the new value
            lists = parseContent.getDataObj(response);

            //DIET TYPE
            dietTypeList = findViewById(R.id.dietTypeList);
            Log.d("dietTypeList", String.valueOf(dietTypeList));

            DataItem dietTypeItem = new DataItem();
            dietTypeItem.setCategoryId("diet_type");
            dietTypeItem.setCategoryName("Diet Type");

            dietTypeArCategory = new ArrayList<>();
            dietTypeArSubCategory = new ArrayList<>();
            dietTypeParentItems = new ArrayList<>();
            dietTypeChildItems = new ArrayList<>();

            try {
                dietTypes = lists.getJSONArray(AndyConstants.Params.ALL);
                newDietType = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < dietTypes.length(); i++) {
                JSONObject dietType = null;
                try {
                    dietType = dietTypes.getJSONObject(i);
                    SubCategoryItem subCategoryItem = new SubCategoryItem();
                    subCategoryItem.setCategoryId("diet_type");
                    subCategoryItem.setSubId(String.valueOf(dietType.getInt(AndyConstants.Params.DT_ID)));

                    if(newDietType.getInt(AndyConstants.Params.DT_ID) == dietType.getInt(AndyConstants.Params.DT_ID)){//if the dietType is the new one
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    subCategoryItem.setSubCategoryName(dietType.getString(AndyConstants.Params.DB_DT_NAME));
                    dietTypeArSubCategory.add(subCategoryItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            dietTypeItem.setSubCategory(dietTypeArSubCategory);
            dietTypeArCategory.add(dietTypeItem);

            for (DataItem data : dietTypeArCategory) {
                //                        Log.i("Item", String.valueOf(arCategory));
                ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> mapParent = new HashMap<String, String>();

                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                int countIsChecked = 0;
                for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                    HashMap<String, String> mapChild = new HashMap<String, String>();
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                    mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                    mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                    if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                        countIsChecked++;
                    }
                    childArrayList.add(mapChild);
                }

                if (countIsChecked == data.getSubCategory().size()) {

                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                } else {
                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                dietTypeChildItems.add(childArrayList);
                dietTypeParentItems.add(mapParent);

            }

            ExpandableConstantManager.parentItems = dietTypeParentItems;
            ExpandableConstantManager.childItems = dietTypeChildItems;

            Log.d("TAG", "dietType parent: " + dietTypeParentItems);
            Log.d("TAG", "dietType children: " + dietTypeChildItems);

            dietTypeExpandableListAdapter = new DietPrefsExpandableListAdapter(this, dietTypeParentItems, dietTypeChildItems, false);

            dietTypeList.setAdapter(dietTypeExpandableListAdapter);

            Log.d("TAG", "dietType parent: " + dietTypeParentItems);
            Log.d("TAG", "dietType children: " + dietTypeChildItems);

            dietTypeList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int height = 0;
                    Log.d("dietType children","count= "+dietTypeList.getChildCount());
                    for (int i = 0; i < dietTypeList.getChildCount(); i++) {
                        childHeight = dietTypeList.getChildAt(i).getMeasuredHeight();
                        height = childHeight + dietTypeList.getDividerHeight();
                    }

                    Log.d("count", String.valueOf(dietTypeExpandableListAdapter.getChildrenCount(groupPosition)));
                    dietTypeList.getLayoutParams().height = (height) * (dietTypeExpandableListAdapter.getChildrenCount(groupPosition)+1);
                }
            });

            // Listview Group collapsed listener
            dietTypeList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                    dietTypeList.getLayoutParams().height = childHeight;
                }
            });

        }else {
            Toast.makeText(AddDishActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }


    public void createCuisineModal()throws IOException, JSONException {
        Log.d("here","inside create cuisine modal");
        cuisineDialogBuilder = new AlertDialog.Builder(this).create();
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.custom_add_dialog, null);

        TextView modalTitle = dialogView.findViewById(R.id.modalTitle);
        modalTitle.setText("Add Cuisine");
        cuisineName = dialogView.findViewById(R.id.name);
        Button submitButton = dialogView.findViewById(R.id.submitButton);
        Button cancelButton = dialogView.findViewById(R.id.cancelButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cuisineDialogBuilder.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addCuisine();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cuisineDialogBuilder.dismiss();
            }
        });

        cuisineDialogBuilder.setView(dialogView);
        cuisineDialogBuilder.show();

    }


    public void addCuisine()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(AddDishActivity.this)) {
            Toast.makeText(AddDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(AddDishActivity.this);
        final HashMap<String, String> cuisineMap = new HashMap<>();
        cuisineMap.put(AndyConstants.Params.DB_CU_NAME,cuisineName.getText().toString());//get the cuisine name they put in

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.CUISINEADD);

                    cuisineMap.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(cuisineMap).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("cuisine res:", result);
                afterAddCuisine(result);
            }
        }.execute();
    }

    private void afterAddCuisine(String response) {
        Log.d("cuisinejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        if (parseContent.isSuccess(response)) {
            Toast.makeText(AddDishActivity.this, "Cuisine added!", Toast.LENGTH_SHORT).show();
            cuisineDialogBuilder.dismiss();

            //repopulate the cuisine list and select the new value
            lists = parseContent.getDataObj(response);

            //CUISINES
            cuisineList = findViewById(R.id.cuisineList);
            Log.d("cuisineList", String.valueOf(cuisineList));

            DataItem cuisineItem = new DataItem();
            cuisineItem.setCategoryId("cuisine");
            cuisineItem.setCategoryName("Cuisine");

            cuisineArCategory = new ArrayList<>();
            cuisineArSubCategory = new ArrayList<>();
            cuisineParentItems = new ArrayList<>();
            cuisineChildItems = new ArrayList<>();

            try {
                cuisines = lists.getJSONArray(AndyConstants.Params.ALL);
                newCuisine = lists.getJSONArray(AndyConstants.Params.SELECTED).getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < cuisines.length(); i++) {
                JSONObject cuisine = null;
                try {
                    cuisine = cuisines.getJSONObject(i);
                    SubCategoryItem subCategoryItem = new SubCategoryItem();
                    subCategoryItem.setCategoryId("cuisine");
                    subCategoryItem.setSubId(String.valueOf(cuisine.getInt(AndyConstants.Params.CU_ID)));

                    if (newCuisine.getInt(AndyConstants.Params.CU_ID) == cuisine.getInt(AndyConstants.Params.CU_ID)) {//if the cuisine is the new one
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                    } else {
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }

                    subCategoryItem.setSubCategoryName(cuisine.getString(AndyConstants.Params.DB_CU_NAME));
                    cuisineArSubCategory.add(subCategoryItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            cuisineItem.setSubCategory(cuisineArSubCategory);
            cuisineArCategory.add(cuisineItem);

            for (DataItem data : cuisineArCategory) {
//                        Log.i("Item", String.valueOf(arCategory));
                ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> mapParent = new HashMap<String, String>();

                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID, data.getCategoryId());
                mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME, data.getCategoryName());

                int countIsChecked = 0;
                for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                    HashMap<String, String> mapChild = new HashMap<String, String>();
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_ID, subCategoryItem.getSubId());
                    mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                    mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID, subCategoryItem.getCategoryId());
                    mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED, subCategoryItem.getIsChecked());

                    if (subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                        countIsChecked++;
                    }
                    childArrayList.add(mapChild);
                }

                if (countIsChecked == data.getSubCategory().size()) {

                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                } else {
                    data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                }

                mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED, data.getIsChecked());
                cuisineChildItems.add(childArrayList);
                cuisineParentItems.add(mapParent);

            }

            ExpandableConstantManager.parentItems = cuisineParentItems;
            ExpandableConstantManager.childItems = cuisineChildItems;

            Log.d("TAG", "cuisine parent: " + cuisineParentItems);
            Log.d("TAG", "cuisine children: " + cuisineChildItems);

            cuisineExpandableListAdapter = new CuisineExpandableListAdapter(this, cuisineParentItems, cuisineChildItems, false);

            cuisineList.setAdapter(cuisineExpandableListAdapter);

            Log.d("TAG", "cuisine parent: " + cuisineParentItems);
            Log.d("TAG", "cuisine children: " + cuisineChildItems);

            cuisineList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    int height = 0;
                    for (int i = 0; i < cuisineList.getChildCount(); i++) {
                        childHeight = cuisineList.getChildAt(i).getMeasuredHeight();
                        height = childHeight + cuisineList.getDividerHeight();
                    }
                    cuisineList.getLayoutParams().height = (height) * (cuisineExpandableListAdapter.getChildrenCount(groupPosition) + 1);
                }
            });

            // Listview Group collapsed listener
            cuisineList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                    cuisineList.getLayoutParams().height = childHeight;
                }
            });

        } else {
            Toast.makeText(AddDishActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }


        //method to show file chooser
        private void showFileChooser() {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        }

        //handling the image chooser activity result
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                path = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                    image.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //method to get the file path from uri
        public String getPath(Uri uri) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            return path;
        }

        //Requesting permission
        private void requestStoragePermission() {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                return;

            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }


        //This method will be called when the user will tap on allow or deny
        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

            //Checking the request code of our request
            if (requestCode == STORAGE_PERMISSION_CODE) {

                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Displaying a toast
                    Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
                } else {
                    //Displaying another toast if permission is not granted
                    Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
                }
            }
        }




        public void addDish(){
            if (!AndyUtils.isNetworkAvailable(AddDishActivity.this)) {
                Toast.makeText(AddDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("Authorization","Basic "+preferenceHelper.getUserPass());
            map.put("url", AndyConstants.ServiceType.DISHESADD);
            map.put(AndyConstants.Params.RID,preferenceHelper.getRID());
            map.put(AndyConstants.Params.DISH_NAME,name.getText().toString());
            map.put(AndyConstants.Params.DESCRIPTION,description.getText().toString());
            map.put(AndyConstants.Params.PRICE,price.getText().toString());
            map.put("image", getPath(path));


            //get the selected values
            //selectedMenu
            for (int i = 0; i < MenuExpandableListAdapter.parentItems.size(); i++) {
                ArrayList<Integer> children = new ArrayList<Integer>();

                for (int j = 0; j < MenuExpandableListAdapter.childItems.get(i).size(); j++) {

                    String isChildChecked = MenuExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                    Integer childID = Integer.parseInt(MenuExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                    if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        children.add(childID);
                    }
                }
                selectedMenu = String.valueOf(children);
                Log.d("selectedMenu",selectedMenu);
            }
            map.put(AndyConstants.Params.MENU_ID,selectedMenu);

            //selectedFoodCategory
            for (int i = 0; i < FoodCategoryExpandableListAdapter.parentItems.size(); i++) {
                ArrayList<Integer> children = new ArrayList<Integer>();

                for (int j = 0; j < FoodCategoryExpandableListAdapter.childItems.get(i).size(); j++) {

                    String isChildChecked = FoodCategoryExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                    Integer childID = Integer.parseInt(FoodCategoryExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                    if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        children.add(childID);
                    }
                }
                selectedFoodCategory = String.valueOf(children);
                Log.d("selectedFoodCategory",selectedFoodCategory);
            }
            map.put(AndyConstants.Params.FC_ID,selectedFoodCategory);

            //selectedDietType
            for (int i = 0; i < DietPrefsExpandableListAdapter.parentItems.size(); i++) {
                ArrayList<Integer> children = new ArrayList<Integer>();

                for (int j = 0; j < DietPrefsExpandableListAdapter.childItems.get(i).size(); j++) {

                    String isChildChecked = DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                    Integer childID = Integer.parseInt(DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                    if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        children.add(childID);
                    }
                }
                selectedDietType = String.valueOf(children);
                Log.d("selectedDietType",selectedDietType);
            }
            map.put(AndyConstants.Params.DT_ID,selectedDietType);

            //selectedCuisine
            for (int i = 0; i < CuisineExpandableListAdapter.parentItems.size(); i++) {
                ArrayList<Integer> children = new ArrayList<Integer>();

                for (int j = 0; j < CuisineExpandableListAdapter.childItems.get(i).size(); j++) {

                    String isChildChecked = CuisineExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                    Integer childID = Integer.parseInt(CuisineExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                    if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                        children.add(childID);
                    }
                }
                selectedCuisine = String.valueOf(children);
                Log.d("selectedCuisine",selectedCuisine);
            }
            map.put(AndyConstants.Params.CU_ID,selectedCuisine);

            new MultiPartRequester(AddDishActivity.this, map, GALLERY,  AddDishActivity.this);
            AndyUtils.showSimpleProgressDialog(this);
        }

        @Override
        public void onTaskCompleted(String response, int serviceCode) {
            AndyUtils.removeSimpleProgressDialog();
            Log.d("res", response.toString());

            if (parseContent.isSuccess(response)) {
                Toast.makeText(AddDishActivity.this, "Dish successfully created!", Toast.LENGTH_SHORT).show();
                String d_id = null;
                try {
                    d_id = parseContent.getDataObj(response).getString(AndyConstants.Params.D_ID);
                    Intent intent = new Intent(AddDishActivity.this, DetailsDishActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(AndyConstants.Params.D_ID,d_id);
                    Log.d("d_id",d_id);
                    startActivity(intent);
                    this.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else {
                Toast.makeText(AddDishActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
            }

        }

    }
