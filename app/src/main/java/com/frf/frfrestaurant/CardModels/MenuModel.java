package com.frf.frfrestaurant.CardModels;

import android.text.Html;

public class MenuModel {

    private String meal_name;
    private String image_url;
    private String cuisine;
    private String diet_type;
    private String start_time;
    private String end_time;
    private String menu_id;

    public String getMealName() {
        return meal_name;
    }

    public void setMealName(String meal_name) {
        this.meal_name =  String.valueOf(Html.fromHtml(meal_name));
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String url) {
        this.image_url =  String.valueOf(Html.fromHtml(url));
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine =  String.valueOf(Html.fromHtml(cuisine));
    }

    public String getDietType() {
        return diet_type;
    }

    public void setDietType(String diet_type) {
        this.diet_type =  String.valueOf(Html.fromHtml(diet_type));
    }

    public String getStartTime() {
        return start_time;
    }

    public void setStartTime(String start) {
        this.start_time = start;
    }

    public String getEndTime() {
        return end_time;
    }

    public void setEndTime(String end) {
        this.end_time = end;
    }

    public String getMenuID() {
        return menu_id;
    }

    public void setMenuID(String id) {
        this.menu_id = id;
    }

}
