package com.frf.frfrestaurant.CardModels;

import android.text.Html;

public class RatingsModel {

    private String username;
    private String rating;
    private String comment;
    private String created_at;
    private String updated_at;

    public String getUsername() {
        return username;
    }

    public void setUsername(String uname) {
        this.username =  String.valueOf(Html.fromHtml(uname));
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment =  String.valueOf(Html.fromHtml(comment));
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String date) {
        this.created_at = date;
    }

    public String getUpdatedAt() {
        return updated_at;
    }

    public void setUpdatedAt(String date) {
        this.updated_at = date;
    }

}
