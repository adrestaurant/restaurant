package com.frf.frfrestaurant.CardModels;


import android.text.Html;

public class DishModel {

    private String dish_name;
    private String food_category;
    private String cuisine;
    private String diet_type;
    private String price;
    private String image_url;
    private String d_id;

    public String getDishName() {
        return dish_name;
    }

    public void setDishName(String dish_name) {
        this.dish_name =  String.valueOf(Html.fromHtml(dish_name));
    }

    public String getFoodCategory() {
        return food_category;
    }

    public void setFoodCategory(String food_category) {
        this.food_category =  String.valueOf(Html.fromHtml(food_category));
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine =  String.valueOf(Html.fromHtml(cuisine));
    }

    public String getDietType() {
        return diet_type;
    }

    public void setDietType(String diet_type) {
        this.diet_type =  String.valueOf(Html.fromHtml(diet_type));
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price =  String.valueOf(Html.fromHtml(price));
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String url) {
        this.image_url =  String.valueOf(Html.fromHtml(url));
    }

    public String getDID() {
        return d_id;
    }

    public void setDID(String id) {
        this.d_id = id;
    }

}
