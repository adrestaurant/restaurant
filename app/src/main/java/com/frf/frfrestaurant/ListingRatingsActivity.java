package com.frf.frfrestaurant;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfrestaurant.CardAdapters.RatingsCardAdapter;
import com.frf.frfrestaurant.CardModels.RatingsModel;
import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListingRatingsActivity extends AppCompatActivity {

    private LinearLayout averageLayout;
    private RatingBar average;
    private TextView count;
    private RecyclerView recyclerView;

    private ArrayList<RatingsModel> ratingsModelArrayList;
    private RatingsCardAdapter adapter;
    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private JSONObject res, count_average;
    private JSONArray ratings;

    private HashMap<String, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_ratings);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        averageLayout = findViewById(R.id.averageLayout);

        average = findViewById(R.id.average);
        average.setIsIndicator(true);

        count = findViewById(R.id.count);

        recyclerView = findViewById(R.id.recyclerView);


        new FetchRatingsAsyncTask().execute();

        averageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ListingRatingsActivity.this, average.getRating()+"/5", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Toast.makeText(ListingRatingsActivity.this, ratingsModelArrayList.get(position).getRating()+"/5", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    /**
     * Fetches the list of restaurants from the server (based on diet preferences)
     */
    private class FetchRatingsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ListingRatingsActivity.this)) {
                Toast.makeText(ListingRatingsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ListingRatingsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RATINGSINDEX);
                HashMap<String, String> map = new HashMap<>();

                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("ratingsjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            if(parseContent.getDataObj(result) == null){//if there are no results show the text view
                showNoResults();
            }
            else {//if there are results, show them
                populateList(result);
            }
        }

    }

    private void showNoResults(){
        TextView noResultsTV = findViewById(R.id.noResultsTV);
        noResultsTV.setVisibility(View.VISIBLE);
    }


    private ArrayList<RatingsModel> populateList(String result){

        ratingsModelArrayList = new ArrayList<RatingsModel>();

        Log.d("received rating info", result);
        if (parseContent.isSuccess(result)) {
            res = parseContent.getDataObj(result);
            try {
                ratings = res.getJSONArray(AndyConstants.Params.RATINGS);
                count_average = (JSONObject) res.get("count_average");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                count.setText(count_average.getString("count"));
                average.setRating(Float.parseFloat(count_average.getString("average")));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for(int i = 0; i < ratings.length(); i++) {
                JSONObject rating = null;
                try {
                    rating = ratings.getJSONObject(i);
                    RatingsModel ratingsModel = new RatingsModel();

                    ratingsModel.setUsername(rating.getString(AndyConstants.Params.USERNAME));
                    ratingsModel.setRating(rating.getString(AndyConstants.Params.RATING));
                    ratingsModel.setComment(rating.getString(AndyConstants.Params.COMMENT));
                    ratingsModel.setCreatedAt(rating.getString(AndyConstants.Params.CREATED_AT));
                    ratingsModel.setUpdatedAt(rating.getString(AndyConstants.Params.UPDATED_AT));

                    ratingsModelArrayList.add(ratingsModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d("number of ratings", ratingsModelArrayList.size() + "");

            adapter = new RatingsCardAdapter(this,ratingsModelArrayList);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        }else {//if something goes wrong with the retrieval, put them back at the home screen with an error toast
            Toast.makeText(ListingRatingsActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ListingRatingsActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }


        return ratingsModelArrayList;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
