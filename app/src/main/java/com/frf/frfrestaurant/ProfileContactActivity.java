package com.frf.frfrestaurant;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfrestaurant.Core.AndyConstants;
import com.frf.frfrestaurant.Core.AndyUtils;
import com.frf.frfrestaurant.Core.HttpRequest;
import com.frf.frfrestaurant.Core.ParseContent;
import com.frf.frfrestaurant.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class ProfileContactActivity extends AppCompatActivity {

    private EditText contact_person_f_name, contact_person_l_name, email, telephone_number;
    private Button updateButton;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    private JSONArray details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_contact);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        contact_person_f_name = findViewById(R.id.contact_person_f_name);
        contact_person_l_name = findViewById(R.id.contact_person_l_name);
        email = findViewById(R.id.email);
        telephone_number = findViewById(R.id.telephone_number);

        updateButton = findViewById(R.id.updateButton);


        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    update();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        new FetchContactAsyncTask().execute();

    }

    private class FetchContactAsyncTask extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ProfileContactActivity.this)) {
                Toast.makeText(ProfileContactActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ProfileContactActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILECONTACTINDEX);

                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();

            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("responsejson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            if (parseContent.isSuccess(result)) {
                details = parseContent.getData((result));
                JSONObject dets = null;
                try {
                    dets = details.getJSONObject(0);

                    contact_person_f_name.setText(Html.fromHtml(dets.getString(AndyConstants.Params.CONTACT_PERSON_F_NAME)));
                    contact_person_l_name.setText(Html.fromHtml(dets.getString(AndyConstants.Params.CONTACT_PERSON_L_NAME)));
                    email.setText(Html.fromHtml(dets.getString(AndyConstants.Params.EMAIL)));
                    telephone_number.setText(Html.fromHtml(dets.getString(AndyConstants.Params.TELEPHONE_NUMBER)));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {//if something goes wrong with the retrieval, put them back at the profile home screen with an error toast
                Toast.makeText(ProfileContactActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileContactActivity.this, ProfileHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                ProfileContactActivity.this.finish();
            }
            Log.d("result", String.valueOf(result));
        }
    }


    public void update()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(ProfileContactActivity.this)) {
            Toast.makeText(ProfileContactActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileContactActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.CONTACT_PERSON_F_NAME, contact_person_f_name.getText().toString());
        map.put(AndyConstants.Params.CONTACT_PERSON_L_NAME, contact_person_l_name.getText().toString());
        map.put(AndyConstants.Params.EMAIL, email.getText().toString());
        map.put(AndyConstants.Params.TELEPHONE_NUMBER, telephone_number.getText().toString());



        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILECONTACTUPDATE);

                    map.put(AndyConstants.Params.RID, preferenceHelper.getRID());

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result);
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {

            JSONArray newPass = parseContent.getData(response);
            JSONObject newPwd = null;
            Toast.makeText(ProfileContactActivity.this, "Update Successful!", Toast.LENGTH_SHORT).show();

            new FetchContactAsyncTask().execute();

        }else {
            Toast.makeText(ProfileContactActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }

}
